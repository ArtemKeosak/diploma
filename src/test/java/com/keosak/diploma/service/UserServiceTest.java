package com.keosak.diploma.service;

import com.keosak.diploma.entity.user.User;
import com.keosak.diploma.repository.user.UserRepository;
import com.keosak.diploma.service.user.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService service;

    @MockBean
    UserRepository repository;

    @Test
    public void testValidLoadUserByUsername(){
        String name = "Name";
        User user = new User();
        user.setUsername(name);

        when(repository.findByUsername(name)).thenReturn(user);
        assertEquals(user, service.loadUserByUsername(name));
    }

    @Test
    public void testNotValidLoadUserByUsername(){
        String name = "Name";

        when(repository.findByUsername(name)).thenReturn(null);
        assertThrows(UsernameNotFoundException.class, () -> service.loadUserByUsername(name));
    }

    @Test
    public void testValidSaveUser(){
        String name = "Name";
        User user = new User();
        user.setUsername(name);

        when(repository.findByUsername(name)).thenReturn(user);
        assertFalse(service.saveUser(user));
    }

    @Test
    public void testNotValidSaveUser(){
        String name = "Name";
        User user = new User();
        user.setUsername(name);
        user.setPassword("1234");

        when(repository.findByUsername(name)).thenReturn(null);
        when(repository.save(user)).thenReturn(null);
        assertTrue(service.saveUser(user));
    }

    @Test
    public void testValidDeleteUser(){
        Long id = 1L;
        Optional<User> optional = Optional.of(new User());

        when(repository.findById(id)).thenReturn(optional);
        assertTrue(service.deleteUser(id));
    }

    @Test
    public void testNotValidDeleteUser(){
        Long id = 1L;
        Optional<User> optional = Optional.empty();

        when(repository.findById(id)).thenReturn(optional);
        assertFalse(service.deleteUser(id));
    }

}
