package com.keosak.diploma.validator;

import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import com.keosak.diploma.validation.ValidatorLoadCalculation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ValidatorLoadCalculationTest {

    @Autowired
    ValidatorLoadCalculation validator;

    @Test
    public void testNotValidValidateIdKey(){
        KeyHolder keyHolder = new GeneratedKeyHolder();

        assertThrows(IllegalArgumentException.class, () -> validator.validateId(keyHolder));
    }

    @Test
    public void testValidValidateId(){
        Long id = 5L;

        assertEquals(id, validator.validateId(id));
    }

    @Test
    public void testNotValidValidateId(){
        Long id1 = -1L;
        Long id2 = null;

        assertThrows(IllegalArgumentException.class, () -> validator.validateId(id1));
        assertThrows(IllegalArgumentException.class, () -> validator.validateId(id2));
    }

    @Test
    public void testValidValidateList(){
        List<LoadCalculationEntity> list = new ArrayList<>();
        list.add(new LoadCalculationEntity());

        assertEquals(list, validator.validateList(list));
    }

    @Test
    public void testNotValidValidateList(){
        List<LoadCalculationEntity> list = new ArrayList<>();

        assertThrows(IllegalArgumentException.class, () -> validator.validateList(list));
        assertThrows(IllegalArgumentException.class, () -> validator.validateList(null));
    }
}
