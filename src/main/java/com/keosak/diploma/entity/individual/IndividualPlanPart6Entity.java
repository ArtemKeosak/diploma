package com.keosak.diploma.entity.individual;

import lombok.Data;

@Data
public class IndividualPlanPart6Entity {

    private Long id;
    private Long idHeader;
    private Integer page;
    private Integer partPage;
    private String date;
    private String content;
}
