package com.keosak.diploma.entity.individual;

import lombok.Data;

@Data
public class IndividualPlanFrontPageEntity {

    private Long id;
    private Long idHeader;
    private Integer page;
    private String year;
    private String position;
    private String degree;
    private String academicStatus;
    private Double shareRate;
    private String note;
}
