package com.keosak.diploma.entity.individual;

import lombok.Data;

@Data
public class IndividualPlanHeadEntity {

    private Long id;
    private Long idUser;
    private String headerName;
    private String university;
    private String institute;
    private String department;
    private String fullName;
}
