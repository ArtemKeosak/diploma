package com.keosak.diploma.entity.individual;

import lombok.Data;

@Data
public class IndividualPlanPart1Entity {

    private Long id;
    private Long idHeader;
    private Integer page;
    private Integer partPage;
    private String nameDiscipline;
    private String institute;
    private Integer course;
    private Integer countStudent;
    private String cipherGroup;
    private Integer lecture;
    private Integer practice;
    private Integer laboratory;
    private Integer seminars;
    private Integer individual;
    private Integer consultations;
    private Integer exConsultation;
    private Integer audTest;
    private Integer indTest;
    private Integer essays;
    private Integer graphicWork;
    private Integer coursework;
    private Integer offset;
    private Integer exSemester;
    private Integer controlPract;
    private Integer exState;
    private Integer controlDipl;
    private Integer controlAsp;
}
