package com.keosak.diploma.entity.individual;

import lombok.Data;

@Data
public class IndividualPlanPart3Entity {

    private Long id;
    private Long idHeader;
    private Integer page;
    private Integer partPage;
    private String content;
    private String result;
    private String term;
    private String mark;
}
