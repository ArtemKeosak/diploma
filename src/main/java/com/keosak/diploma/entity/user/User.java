package com.keosak.diploma.entity.user;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "[User]")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Transient
    private String passwordConfirm;
    @Column(name = "fullname")
    private String fullName;
    @Column(name = "startcontract")
    private String startContract;
    @Column(name = "endcontract")
    private String endContract;
    @Column(name = "sharerate")
    private Float shareRate;
    @Column(name = "montsworked")
    private Integer monthsWorked;
    @Transient
    private Long idPosition;
    @OneToOne
    @JoinColumn(name = "idposition", referencedColumnName = "id")
    private Position position;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "userrole",
            joinColumns = { @JoinColumn(name = "iduser") },
            inverseJoinColumns = { @JoinColumn(name = "idrole") })
    private Set<Role> roles;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStartContract() {
        return startContract;
    }

    public void setStartContract(String startContract) {
        this.startContract = startContract;
    }

    public String getEndContract() {
        return endContract;
    }

    public void setEndContract(String endContract) {
        this.endContract = endContract;
    }

    public Float getShareRate() {
        return shareRate;
    }

    public void setShareRate(Float shareRate) {
        this.shareRate = shareRate;
    }

    public Integer getMonthsWorked() {
        return monthsWorked;
    }

    public void setMonthsWorked(Integer monthsWorked) {
        this.monthsWorked = monthsWorked;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Long getIdPosition() {
        return idPosition;
    }

    public void setIdPosition(Long idPosition) {
        this.idPosition = idPosition;
    }
}
