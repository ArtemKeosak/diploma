package com.keosak.diploma.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.keosak.diploma.entity.user.User;

import javax.persistence.*;

@Entity
@Table(name = "Position")
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "fullname")
    private String fullName;
    @Column(name = "shortname")
    private String shortName;
    @OneToOne(mappedBy = "position")
    @JsonIgnore
    private User user;

    public Position() {
    }

    public Position(Long id, String fullName, String shortName) {
        this.id = id;
        this.fullName = fullName;
        this.shortName = shortName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
