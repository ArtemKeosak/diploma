package com.keosak.diploma.entity.load;

import lombok.Data;

@Data
public class LoadCalculationHeadEntity {

    private Long id;
    private Long idUser;
    private String year;
    private String department;
    private String institute;
}
