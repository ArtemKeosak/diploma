package com.keosak.diploma.entity.load.util;

import com.keosak.diploma.entity.load.LoadCalculationHeadEntity;
import com.keosak.diploma.entity.load.LoadCalculationPartBlueEntity;
import lombok.Data;

@Data
public class LoadCalculationEntity {
    LoadCalculationHeadEntity headEntity = new LoadCalculationHeadEntity();
    LoadCalculationPartBlueEntity partBlueEntity = new LoadCalculationPartBlueEntity();
}
