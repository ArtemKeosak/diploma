package com.keosak.diploma.entity.load;

import lombok.Data;

@Data
public class LoadCalculationPartBlueEntity {

    private Long id;
    private Long idHead = 1L;
    private Integer numberSemester;
    private String nameDiscipline;
    private String institute;
    private Integer course;
    private Integer countStudent;
    private String cipherGroup;
    private Integer countStream;
    private Integer countGroup;
    private Integer countSubgroup;
    private Integer fullAmount;
    private Integer lecture;
    private Integer practicalExercises;
    private Integer laboratory;
    private Double individual;
    private Integer practice;
    private Integer dipl;
    private Integer exState;
    private Integer offset;
    private Integer exConsultation;
    private Double exam;
    private Integer countGradStudent;
}
