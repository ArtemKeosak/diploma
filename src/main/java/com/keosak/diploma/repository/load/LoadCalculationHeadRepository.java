package com.keosak.diploma.repository.load;

import com.keosak.diploma.entity.load.LoadCalculationHeadEntity;
import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import static com.keosak.diploma.repository.load.util.LoadCalculationSQL.*;

@Repository
public class LoadCalculationHeadRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<LoadCalculationEntity> mapper = (rs, rowNum) -> {
        LoadCalculationEntity entity = new LoadCalculationEntity();
        entity.getHeadEntity().setId(rs.getLong("h.id"));
        entity.getHeadEntity().setYear(rs.getString("h.year"));
        entity.getHeadEntity().setDepartment(rs.getString("h.department"));
        entity.getHeadEntity().setInstitute(rs.getString("h.institute"));

        entity.getPartBlueEntity().setId(rs.getLong("pb.id"));
        entity.getPartBlueEntity().setIdHead(rs.getLong("pb.idHead"));
        entity.getPartBlueEntity().setNumberSemester(rs.getInt("pb.numberSemester"));
        entity.getPartBlueEntity().setNameDiscipline(rs.getString("pb.nameDiscipline"));
        entity.getPartBlueEntity().setInstitute(rs.getString("pb.institute"));
        entity.getPartBlueEntity().setCourse(rs.getInt("pb.course"));
        entity.getPartBlueEntity().setCountStudent(rs.getInt("pb.countStudent"));
        entity.getPartBlueEntity().setCipherGroup(rs.getString("pb.cipherGroup"));
        entity.getPartBlueEntity().setCountStream(rs.getInt("pb.countStream"));
        entity.getPartBlueEntity().setCountGroup(rs.getInt("pb.countGroup"));
        entity.getPartBlueEntity().setCountSubgroup(rs.getInt("pb.countSubgroup"));
        entity.getPartBlueEntity().setFullAmount(rs.getInt("pb.fullAmount"));
        entity.getPartBlueEntity().setLecture(rs.getInt("pb.lecture"));
        entity.getPartBlueEntity().setPracticalExercises(rs.getInt("pb.practicalExercises"));
        entity.getPartBlueEntity().setLaboratory(rs.getInt("pb.laboratory"));
        entity.getPartBlueEntity().setIndividual(rs.getDouble("pb.individual"));
        entity.getPartBlueEntity().setPractice(rs.getInt("pb.practice"));
        entity.getPartBlueEntity().setDipl(rs.getInt("pb.dipl"));
        entity.getPartBlueEntity().setExState(rs.getInt("pb.exState"));
        entity.getPartBlueEntity().setOffset(rs.getInt("pb.offset"));
        entity.getPartBlueEntity().setExConsultation(rs.getInt("pb.exConsultation"));
        entity.getPartBlueEntity().setExam(rs.getDouble("pb.exam"));
        entity.getPartBlueEntity().setCountGradStudent(rs.getInt("pb.countGradStudent"));
        return entity;
    };

    public KeyHolder addHead(LoadCalculationHeadEntity headEntity){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(ADD_LOAD_CALCULATION_HEAD,
                    Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, headEntity.getIdUser());
            ps.setString(2, headEntity.getYear());
            ps.setString(3, headEntity.getDepartment());
            ps.setString(4, headEntity.getInstitute());
            return ps;
        }, keyHolder);

        return keyHolder;

    }

    public List<LoadCalculationEntity> findHeadByIdUser(Long idUser){
        return jdbcTemplate.query(FIND_LOAD_CALCULATION_HEAD_WITH_PART_BLUE, mapper, idUser);
    }
}
