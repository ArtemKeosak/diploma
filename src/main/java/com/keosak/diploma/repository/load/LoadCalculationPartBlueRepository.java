package com.keosak.diploma.repository.load;

import com.keosak.diploma.entity.load.LoadCalculationPartBlueEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.load.util.LoadCalculationSQL.ADD_ALL_LOAD_CALCULATION_PART_BLUE;

@Repository
public class LoadCalculationPartBlueRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private final RowMapper<LoadCalculationPartBlueEntity> mapper = (rs, rowNum) -> {
        LoadCalculationPartBlueEntity entity = new LoadCalculationPartBlueEntity();
        entity.setId(rs.getLong("id"));
        entity.setNumberSemester(rs.getInt("numberSemester"));
        entity.setNameDiscipline(rs.getString("nameDiscipline"));
        entity.setInstitute(rs.getString("institute"));
        entity.setCourse(rs.getInt("course"));
        entity.setCountStudent(rs.getInt("countStudent"));
        entity.setCipherGroup(rs.getString("cipherGroup"));
        entity.setCountStream(rs.getInt("countStream"));
        entity.setCountGroup(rs.getInt("countGroup"));
        entity.setCountSubgroup(rs.getInt("countSubgroup"));
        entity.setFullAmount(rs.getInt("fullAmount"));
        entity.setLecture(rs.getInt("lecture"));
        entity.setPracticalExercises(rs.getInt("practicalExercises"));
        entity.setLaboratory(rs.getInt("laboratory"));
        entity.setIndividual(rs.getDouble("individual"));
        entity.setPractice(rs.getInt("practice"));
        entity.setDipl(rs.getInt("dipl"));
        entity.setExState(rs.getInt("exState"));
        entity.setOffset(rs.getInt("offset"));
        entity.setExConsultation(rs.getInt("exConsultation"));
        entity.setExam(rs.getDouble("exam"));
        entity.setCountGradStudent(rs.getInt("countGradStudent"));
        return entity;
    };


//    public List<LoadCalculationPartBlueEntity> findAllPartBlue() {
//        return jdbcTemplate.query(FIND_ALL_LOAD_CALCULATION_PART_BLUE, mapper);
//    }

    public void addAllPartBlue(List<LoadCalculationPartBlueEntity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = jdbcTemplate.batchUpdate(ADD_ALL_LOAD_CALCULATION_PART_BLUE, batch);

    }

}
