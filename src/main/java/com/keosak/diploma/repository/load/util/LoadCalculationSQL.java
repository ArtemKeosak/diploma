package com.keosak.diploma.repository.load.util;

public class LoadCalculationSQL {
    public static final String FIND_ALL_LOAD_CALCULATION_PART_BLUE = "Select id, idHead, " +
            "numberSemester, nameDiscipline, institute, course, countStudent, cipherGroup, countStream, " +
            "countGroup, countSubgroup, fullAmount, lecture, practicalExercises, laboratory, " +
            "individual, practice, dipl, exState, offset, exConsultation, exam, countGradStudent " +
            "From LoadCalculationPartBlue";

    public static final String ADD_ALL_LOAD_CALCULATION_PART_BLUE = "INSERT INTO LoadCalculationPartBlue" +
            "(idHead, numberSemester, nameDiscipline, institute, course, countStudent, cipherGroup, " +
            "countStream, countGroup, countSubgroup, fullAmount, lecture, practicalExercises, " +
            "laboratory, individual, practice, dipl, exState, offset, exConsultation, " +
            "exam, countGradStudent)" +
            "VALUES " +
            "(:idHead, :numberSemester, :nameDiscipline, :institute, :course, :countStudent, :cipherGroup, " +
            ":countStream, :countGroup, :countSubgroup, :fullAmount, :lecture, :practicalExercises, " +
            ":laboratory, :individual, :practice, :dipl, :exState, :offset, :exConsultation, " +
            ":exam, :countGradStudent)";

    public static final String ADD_LOAD_CALCULATION_HEAD = "Insert Into LoadCalculationHead" +
            "(idUser, year, department, institute) Values (?, ?, ?, ?)";

    public static final String FIND_LOAD_CALCULATION_HEAD_WITH_PART_BLUE =
            "  Declare @id bigint; \n" +
                    "Select Top 1 @id = id From [LoadCalculationHead] \n" +
                    "Where IdUser = ? Order by id Desc \n" +
                    "Select h.id [h.id], h.year [h.year], h.department [h.department], " +
                    "h.institute [h.institute], pb.id [pb.id], pb.idHead [pb.idHead], " +
                    "pb.numberSemester [pb.numberSemester], pb.nameDiscipline [pb.nameDiscipline], " +
                    "pb.institute [pb.institute], pb.course [pb.course], pb.countStudent [pb.countStudent], " +
                    "pb.cipherGroup [pb.cipherGroup], pb.countStream [pb.countStream], " +
                    "pb.countGroup [pb.countGroup], pb.countSubgroup [pb.countSubgroup], " +
                    "pb.fullAmount [pb.fullAmount], pb.lecture [pb.lecture], " +
                    "pb.practicalExercises [pb.practicalExercises], pb.laboratory [pb.laboratory], " +
                    "pb.individual [pb.individual], pb.practice [pb.practice], pb.dipl [pb.dipl], " +
                    "pb.exState [pb.exState], pb.offset [pb.offset], pb.exConsultation [pb.exConsultation], " +
                    "pb.exam [pb.exam], pb.countGradStudent [pb.countGradStudent] \n" +
                    "From LoadCalculationHead h\n" +
                    "Join LoadCalculationPartBlue pb on pb.IdHead = h.Id\n" +
                    "Where h.id = @id";
}
