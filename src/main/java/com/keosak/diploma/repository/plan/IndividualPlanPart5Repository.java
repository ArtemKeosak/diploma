package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart4Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart5Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.*;

@Repository
public class IndividualPlanPart5Repository {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanPart5Entity> mapper = (rs, rowNum) -> {
        IndividualPlanPart5Entity entity = new IndividualPlanPart5Entity();
        entity.setId(rs.getLong("Id"));
        entity.setIdHeader(rs.getLong("IdHeader"));
        entity.setPage(rs.getInt("Page"));
        entity.setPartPage(rs.getInt("PartPage"));
        entity.setDate(rs.getString("Date"));
        entity.setContent(rs.getString("Content"));
        return entity;
    };

    public void addPart5(List<IndividualPlanPart5Entity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = namedJdbcTemplate.batchUpdate(ADD_PART5, batch);
    }

    public List<IndividualPlanPart5Entity> getByIdHead(Long idHead){
        return jdbcTemplate.query(GET_PART5, mapper, idHead);
    }
}
