package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart5Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart6Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.*;

@Repository
public class IndividualPlanPart6Repository {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanPart6Entity> mapper = (rs, rowNum) -> {
        IndividualPlanPart6Entity entity = new IndividualPlanPart6Entity();
        entity.setId(rs.getLong("Id"));
        entity.setIdHeader(rs.getLong("IdHeader"));
        entity.setPage(rs.getInt("Page"));
        entity.setPartPage(rs.getInt("PartPage"));
        entity.setDate(rs.getString("Date"));
        entity.setContent(rs.getString("Content"));
        return entity;
    };

    public void addPart6(List<IndividualPlanPart6Entity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = namedJdbcTemplate.batchUpdate(ADD_PART6, batch);
    }

    public List<IndividualPlanPart6Entity> getByIdHead(Long idHead){
        return jdbcTemplate.query(GET_PART6, mapper, idHead);
    }

}
