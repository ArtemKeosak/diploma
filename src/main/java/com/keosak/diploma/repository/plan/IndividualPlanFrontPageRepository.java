package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanFrontPageEntity;
import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.*;

@Repository
public class IndividualPlanFrontPageRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanFrontPageEntity> mapper = (rs, rowNum) -> {
        IndividualPlanFrontPageEntity entity = new IndividualPlanFrontPageEntity();
        entity.setId(rs.getLong("Id"));
        entity.setIdHeader(rs.getLong("IdHeader"));
        entity.setPage(rs.getInt("Page"));
        entity.setYear(rs.getString("Year"));
        entity.setPosition(rs.getString("Position"));
        entity.setDegree(rs.getString("Degree"));
        entity.setAcademicStatus(rs.getString("AcademicStatus"));
        entity.setShareRate(rs.getDouble("ShareRate"));
        entity.setNote(rs.getString("Note"));
        return entity;
    };

    public void addFrontPage(List<IndividualPlanFrontPageEntity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = namedJdbcTemplate.batchUpdate(ADD_FRONT_PAGE, batch);
    }


    public List<IndividualPlanFrontPageEntity> getByIdHead(Long idHead){
        return jdbcTemplate.query(GET_FRONT_PAGE, mapper, idHead);
    }
}
