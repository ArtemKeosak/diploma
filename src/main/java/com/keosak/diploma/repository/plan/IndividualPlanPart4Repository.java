package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart2Entity;
import com.keosak.diploma.entity.individual.IndividualPlanPart4Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.*;

@Repository
public class IndividualPlanPart4Repository {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanPart4Entity> mapper = (rs, rowNum) -> {
        IndividualPlanPart4Entity entity = new IndividualPlanPart4Entity();
        entity.setId(rs.getLong("Id"));
        entity.setIdHeader(rs.getLong("IdHeader"));
        entity.setPage(rs.getInt("Page"));
        entity.setPartPage(rs.getInt("PartPage"));
        entity.setContent(rs.getString("Content"));
        entity.setResult(rs.getString("Result"));
        entity.setTerm(rs.getString("Term"));
        entity.setMark(rs.getString("Mark"));
        return entity;
    };

    public void addPart4(List<IndividualPlanPart4Entity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = namedJdbcTemplate.batchUpdate(ADD_PART4, batch);
    }

    public List<IndividualPlanPart4Entity> getByIdHead(Long idHead){
        return jdbcTemplate.query(GET_PART4, mapper, idHead);
    }
}
