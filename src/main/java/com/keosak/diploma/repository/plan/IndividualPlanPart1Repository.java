package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanFrontPageEntity;
import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.*;

@Repository
public class IndividualPlanPart1Repository {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanPart1Entity> mapper = (rs, rowNum) -> {
        IndividualPlanPart1Entity entity = new IndividualPlanPart1Entity();
        entity.setId(rs.getLong("Id"));
        entity.setIdHeader(rs.getLong("IdHeader"));
        entity.setPage(rs.getInt("Page"));
        entity.setPartPage(rs.getInt("PartPage"));
        entity.setNameDiscipline(rs.getString("NameDiscipline"));
        entity.setInstitute(rs.getString("Institute"));
        entity.setCourse(rs.getInt("Course"));
        entity.setCountStudent(rs.getInt("CountStudent"));
        entity.setCipherGroup(rs.getString("CipherGroup"));
        entity.setLecture(rs.getInt("Lecture"));
        entity.setPractice(rs.getInt("Practice"));
        entity.setLaboratory(rs.getInt("Laboratory"));
        entity.setSeminars(rs.getInt("Seminars"));
        entity.setIndividual(rs.getInt("Individual"));
        entity.setConsultations(rs.getInt("Consultations"));
        entity.setExConsultation(rs.getInt("ExConsultation"));
        entity.setAudTest(rs.getInt("AudTest"));
        entity.setIndTest(rs.getInt("IndTest"));
        entity.setEssays(rs.getInt("Essays"));
        entity.setGraphicWork(rs.getInt("GraphicWork"));
        entity.setCoursework(rs.getInt("Coursework"));
        entity.setOffset(rs.getInt("Offset"));
        entity.setExSemester(rs.getInt("ExSemester"));
        entity.setControlPract(rs.getInt("ControlPract"));
        entity.setExState(rs.getInt("ExState"));
        entity.setControlDipl(rs.getInt("ControlDipl"));
        entity.setControlAsp(rs.getInt("ControlAsp"));

        return entity;
    };
    public void addPart1(List<IndividualPlanPart1Entity> empList){
        SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(empList.toArray());
        int[] updateCounts = namedJdbcTemplate.batchUpdate(ADD_PART1, batch);
    }

    public List<IndividualPlanPart1Entity> getByIdHead(Long idHead){
        return jdbcTemplate.query(GET_PART1, mapper, idHead);
    }

    public List<IndividualPlanPart1Entity> getPage1ByIdUserAndYear(Long idUser, String year){
        return jdbcTemplate.query(GET_PAGE1_BY_USER_AND_YEAR, mapper, idUser, year);
    }
}
