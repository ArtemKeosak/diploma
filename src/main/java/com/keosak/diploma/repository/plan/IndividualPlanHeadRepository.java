package com.keosak.diploma.repository.plan;

import com.keosak.diploma.entity.individual.IndividualPlanHeadEntity;
import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.ADD_HEAD;
import static com.keosak.diploma.repository.plan.util.IndividualPlanSQL.GET_HEAD;

@Repository
public class IndividualPlanHeadRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<IndividualPlanHeadEntity> mapper = (rs, rowNum) -> {
        IndividualPlanHeadEntity entity = new IndividualPlanHeadEntity();
        entity.setId(rs.getLong("Id"));
        entity.setIdUser(rs.getLong("IdUser"));
        entity.setHeaderName(rs.getString("HeaderName"));
        entity.setUniversity(rs.getString("University"));
        entity.setInstitute(rs.getString("Institute"));
        entity.setDepartment(rs.getString("Department"));
        entity.setFullName(rs.getString("FullName"));
        return entity;
    };

    public KeyHolder addHead(IndividualPlanHeadEntity headEntity){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(ADD_HEAD,
                    Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, headEntity.getIdUser());
            ps.setString(2, headEntity.getHeaderName());
            ps.setString(3, headEntity.getUniversity());
            ps.setString(4, headEntity.getInstitute());
            ps.setString(5, headEntity.getDepartment());
            ps.setString(6, headEntity.getFullName());
            return ps;
        }, keyHolder);

        return keyHolder;
    }

    public List<IndividualPlanHeadEntity> getByIdUser(Long idUser){
        return jdbcTemplate.query(GET_HEAD, mapper, idUser);
    }

}
