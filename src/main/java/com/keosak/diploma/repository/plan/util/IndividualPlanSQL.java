package com.keosak.diploma.repository.plan.util;

public class IndividualPlanSQL {
    public static final String ADD_HEAD = "Insert into IndPlanHeader" +
            "(IdUser, HeaderName, University, Institute, Department, FullName) " +
            "Values (?, ?, ?, ?, ?, ?)";
    public static final String ADD_FRONT_PAGE = "INSERT INTO IndPlanFrontPage" +
            "(IdHeader, Page, Year, Position, Degree, AcademicStatus, " +
            "ShareRate, Note) " +
            "VALUES " +
            "(:idHeader, :page, :year, :position, :degree, :academicStatus, " +
            ":shareRate, :note)";
    public static final String ADD_PART1 = "INSERT INTO IndPlanPart1" +
            "(IdHeader, Page, PartPage, NameDiscipline, Institute, Course, " +
            "CountStudent, CipherGroup, Lecture, Practice, Laboratory, " +
            "Seminars, Individual, Consultations, ExConsultation, AudTest, " +
            "IndTest, Essays, GraphicWork, Coursework, Offset, ExSemester, " +
            "ControlPract, ExState, ControlDipl, ControlAsp) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :nameDiscipline, :institute, " +
            ":course, :countStudent, :cipherGroup, :lecture, :practice, " +
            ":laboratory, :seminars, :individual, :consultations, " +
            ":exConsultation, :audTest, :indTest, :essays, :graphicWork, " +
            ":coursework, :offset, :exSemester, :controlPract, :exState, " +
            ":controlDipl, :controlAsp) ";
    public static final String ADD_PART2 = "INSERT INTO IndPlanPart2" +
            "(IdHeader, Page, PartPage, Content, Result, Term, Mark) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :content, :result, :term, " +
            ":mark)";
    public static final String ADD_PART3 = "INSERT INTO IndPlanPart3" +
            "(IdHeader, Page, PartPage, Content, Result, Term, Mark) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :content, :result, :term, " +
            ":mark)";
    public static final String ADD_PART4 = "INSERT INTO IndPlanPart4" +
            "(IdHeader, Page, PartPage, Content, Result, Term, Mark) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :content, :result, :term, " +
            ":mark)";
    public static final String ADD_PART5 = "INSERT INTO IndPlanPart5" +
            "(IdHeader, Page, PartPage, Date, Content) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :date, :content)";
    public static final String ADD_PART6 = "INSERT INTO IndPlanPart6" +
            "(IdHeader, Page, PartPage, Date, Content) " +
            "VALUES " +
            "(:idHeader, :page, :partPage, :date, :content)";


    private static final String HEAD = " Declare @id bigint; " +
            " Select Top 1 @id = id From IndPlanHeader " +
            " Where IdUser = ? Order by id Desc ";
    public static final String GET_HEAD = HEAD + " Select Id, IdUser, HeaderName, " +
            " University, Institute, Department, FullName From IndPlanHeader " +
            " Where Id = @id ";
    public static final String GET_FRONT_PAGE = " Select Id, IdHeader, Page, Year, " +
            " Position, Degree, AcademicStatus, ShareRate, Note From IndPlanFrontPage" +
            " Where IdHeader = ? ";
    public static final String GET_PART1 = " Select Id, IdHeader, " +
            " Page, PartPage, NameDiscipline, Institute, Course, CountStudent, " +
            " CipherGroup, Lecture, Practice, Laboratory, Seminars, " +
            " Individual, Consultations, ExConsultation, AudTest, IndTest, " +
            " Essays, GraphicWork, Coursework, Offset, ExSemester, ControlPract, " +
            " ExState, ControlDipl, ControlAsp From IndPlanPart1" +
            " Where IdHeader = ? ";
    public static final String GET_PART2 = " Select Id, IdHeader, " +
            " Page, PartPage, Content, Result, Term, Mark From IndPlanPart2" +
            " Where IdHeader = ? ";
    public static final String GET_PART3 = " Select Id, IdHeader, " +
            " Page, PartPage, Content, Result, Term, Mark From IndPlanPart3" +
            " Where IdHeader = ? ";
    public static final String GET_PART4 = " Select Id, IdHeader, " +
            " Page, PartPage, Content, Result, Term, Mark From IndPlanPart4" +
            " Where IdHeader = ? ";
    public static final String GET_PART5 = " Select Id, IdHeader, " +
            " Page, PartPage, Date, Content From IndPlanPart5" +
            " Where IdHeader = ? ";
    public static final String GET_PART6 = " Select Id, IdHeader, " +
            " Page, PartPage, Date, Content From IndPlanPart6" +
            " Where IdHeader = ? ";

    public static final String GET_PAGE1_BY_USER_AND_YEAR = "Declare @id bigint; " +
            " Select Top 1 @id = id From IndPlanHeader" +
            " Where IdUser = ? Order by id Desc " +
            " Select p.Id [Id], p.IdHeader [IdHeader], p.Page [Page], " +
            " p.PartPage [PartPage], p.NameDiscipline [NameDiscipline], " +
            " p.Institute [Institute], p.Course [Course], p.CountStudent [CountStudent], " +
            " p.CipherGroup [CipherGroup], p.Lecture [Lecture], p.Practice [Practice], " +
            " p.Laboratory [Laboratory], p.Seminars [Seminars], " +
            " p.Individual [Individual], p.Consultations [Consultations], " +
            " p.ExConsultation [ExConsultation], p.AudTest [AudTest], " +
            " p.IndTest [IndTest], p.Essays [Essays], p.GraphicWork [GraphicWork], " +
            " p.Coursework [Coursework], p.Offset [Offset], p.ExSemester [ExSemester], " +
            " p.ControlPract [ControlPract], p.ExState [ExState], " +
            " p.ControlDipl [ControlDipl], p.ControlAsp [ControlAsp] From IndPlanPart1 p " +
            " Join IndPlanFrontPage f ON f.IdHeader = p.IdHeader" +
            " Where p.IdHeader = @id  AND f.Page = p.Page AND f.year = ? ";
}
