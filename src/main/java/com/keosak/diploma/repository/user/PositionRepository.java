package com.keosak.diploma.repository.user;

import com.keosak.diploma.entity.user.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long>{

}
