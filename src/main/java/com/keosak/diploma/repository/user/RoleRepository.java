package com.keosak.diploma.repository.user;


import com.keosak.diploma.entity.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
