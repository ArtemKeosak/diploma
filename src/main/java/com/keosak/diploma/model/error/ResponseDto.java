package com.keosak.diploma.model.error;

import java.util.List;

public class ResponseDto<T> {
    T data;
    List<ErrorDto> errors;

    public ResponseDto() {
    }

    public ResponseDto(T data) {
        this.data = data;
    }



    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<ErrorDto> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDto> errors) {
        this.errors = errors;
    }
}

