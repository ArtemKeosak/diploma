package com.keosak.diploma.model.error;

import lombok.Data;

@Data
public class ResultDto {
    private String alertMsg;
    private String errorMsg;
}
