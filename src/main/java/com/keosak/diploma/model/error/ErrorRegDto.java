package com.keosak.diploma.model.error;

import lombok.Data;

@Data
public class ErrorRegDto {

    private String nameField;
    private String nameError;

    public ErrorRegDto(String nameField, String nameError) {
        this.nameField = nameField;
        this.nameError = nameError;
    }
}
