package com.keosak.diploma.model.individual;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class IndividualPlanHeadDto {

    private Long id;
    private Long idUser;
    private String headerName;
    private String university;
    private String institute;
    private String department;
    private String fullName;
    private List<IndividualPlanFrontPageDto> frontPages = new ArrayList<>();
    private List<IndividualPlanPart1Dto> part1 = new ArrayList<>();
    private List<IndividualPlanPart2Dto> part2 = new ArrayList<>();
    private List<IndividualPlanPart3Dto> part3 = new ArrayList<>();
    private List<IndividualPlanPart4Dto> part4 = new ArrayList<>();
    private List<IndividualPlanPart5Dto> part5 = new ArrayList<>();
    private List<IndividualPlanPart6Dto> part6 = new ArrayList<>();
}
