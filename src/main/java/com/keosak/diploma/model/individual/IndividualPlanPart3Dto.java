package com.keosak.diploma.model.individual;

import lombok.Data;

@Data
public class IndividualPlanPart3Dto {

    private Long id;
    private Integer page;
    private Integer partPage;
    private String content;
    private String result;
    private String term;
    private String mark;

    public void setData(int id, Object data) {
        switch (id) {
            case 1:
                content = (String) data;
                break;
            case 2:
                result = (String) data;
                break;
            case 3:
                term = (String) data;
                break;
            case 4:
                mark = (String) data;
                break;
        }
    }

    public Object getData(int id) {
        Object data = null;

        switch (id) {
            case 1:
                data = content;
                break;
            case 2:
                data = result;
                break;
            case 3:
                data = term;
                break;
            case 4:
                data = mark;
                break;
        }
        return data;
    }
}
