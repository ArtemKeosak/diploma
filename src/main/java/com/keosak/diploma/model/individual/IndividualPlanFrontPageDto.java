package com.keosak.diploma.model.individual;

import lombok.Data;

@Data
public class IndividualPlanFrontPageDto {

    private Long id;
    private Integer page;
    private String year;
    private String position;
    private String degree;
    private String academicStatus;
    private Double shareRate;
    private String note;

    public void setData(int id, Object data) {
        switch (id) {
            case 0:
                year = (String) data;
                break;
            case 1:
                position = (String) data;
                break;
            case 2:
                degree = (String) data;
                break;
            case 3:
                academicStatus = (String) data;
                break;
            case 4:
                shareRate = (Double) data;
                break;
            case 5:
                note = (String) data;
                break;
        }
    }

    public Object getData(int id) {
        Object data = null;

        switch (id) {
            case 0:
                data = year;
                break;
            case 1:
                data = position;
                break;
            case 2:
                data = degree;
                break;
            case 3:
                data = academicStatus;
                break;
            case 4:
                data = shareRate;
                break;
            case 5:
                data = note;
                break;
        }
        return data;
    }
}
