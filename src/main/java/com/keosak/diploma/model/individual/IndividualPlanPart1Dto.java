package com.keosak.diploma.model.individual;

import lombok.Data;

@Data
public class IndividualPlanPart1Dto {

    private Long id;
    private Integer page;
    private Integer partPage;
    private String nameDiscipline;
    private String institute;
    private Integer course;
    private Integer countStudent;
    private String cipherGroup;
    private Integer lecture;
    private Integer practice;
    private Integer laboratory;
    private Integer seminars;
    private Integer individual;
    private Integer consultations;
    private Integer exConsultation;
    private Integer audTest;
    private Integer indTest;
    private Integer essays;
    private Integer graphicWork;
    private Integer coursework;
    private Integer offset;
    private Integer exSemester;
    private Integer controlPract;
    private Integer exState;
    private Integer controlDipl;
    private Integer controlAsp;


    public void setData(int id, Object data) {
        if (data == null || data.equals("")) {
            return;
        }

        switch (id) {
            case 2:
                nameDiscipline = (String) data;
                break;
            case 3:
                institute = (String) data;
                break;
            case 4:
                course = ((Double) data).intValue();
                break;
            case 5:
                countStudent = ((Double) data).intValue();
                break;
            case 6:
                cipherGroup = (String) data;
                break;
            case 7:
                lecture = ((Double) data).intValue();
                break;
            case 8:
                practice = ((Double) data).intValue();
                break;
            case 9:
                laboratory = ((Double) data).intValue();
                break;
            case 10:
                seminars = ((Double) data).intValue();
                break;
            case 11:
                individual = ((Double) data).intValue();
                break;
            case 12:
                consultations = ((Double) data).intValue();
                break;
            case 13:
                exConsultation = ((Double) data).intValue();
                break;
            case 14:
                audTest = ((Double) data).intValue();
                break;
            case 15:
                indTest = ((Double) data).intValue();
                break;
            case 16:
                essays = ((Double) data).intValue();
                break;
            case 17:
                graphicWork = ((Double) data).intValue();
                break;
            case 18:
                coursework = ((Double) data).intValue();
                break;
            case 19:
                offset = ((Double) data).intValue();
                break;
            case 20:
                exSemester = ((Double) data).intValue();
                break;
            case 21:
                controlPract = ((Double) data).intValue();
                break;
            case 22:
                exState = ((Double) data).intValue();
                break;
            case 23:
                controlDipl = ((Double) data).intValue();
                break;
            case 24:
                controlAsp = ((Double) data).intValue();
                break;
        }
    }

    public Object getData(int id) {
        Object result = null;

        switch (id) {
            case 2:
                result = nameDiscipline;
                break;
            case 3:
                result = institute;
                break;
            case 4:
                result = course;
                break;
            case 5:
                result = countStudent;
                break;
            case 6:
                result = cipherGroup;
                break;
            case 7:
                result = lecture;
                break;
            case 8:
                result = practice;
                break;
            case 9:
                result = laboratory;
                break;
            case 10:
                result = seminars;
                break;
            case 11:
                result = individual;
                break;
            case 12:
                result = consultations;
                break;
            case 13:
                result = exConsultation;
                break;
            case 14:
                result = audTest;
                break;
            case 15:
                result = indTest;
                break;
            case 16:
                result = essays;
                break;
            case 17:
                result = graphicWork;
                break;
            case 18:
                result = coursework;
                break;
            case 19:
                result = offset;
                break;
            case 20:
                result = exSemester;
                break;
            case 21:
                result = controlPract;
                break;
            case 22:
                result = exState;
                break;
            case 23:
                result = controlDipl;
                break;
            case 24:
                result = controlAsp;
                break;
        }
        return result;
    }

}
