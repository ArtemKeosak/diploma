package com.keosak.diploma.model.individual;

import lombok.Data;

@Data
public class IndividualPlanPart6Dto {

    private Long id;
    private Integer page;
    private Integer partPage;
    private String date;
    private String content;

    public void setData(int id, Object data) {
        switch (id) {
            case 0:
                date = (String) data;
                break;
            case 1:
                content = (String) data;
                break;
        }
    }

    public Object getData(int id) {
        Object result = null;

        switch (id) {
            case 0:
                result = date;
                break;
            case 1:
                result = content;
                break;
        }
        return result;
    }
}
