package com.keosak.diploma.model.load;

import lombok.Data;

@Data
public class LoadCalculationPartBlueDto {

    private Long id;
    private Integer numberSemester = 0;
    private String nameDiscipline = "";
    private String institute = "";
    private Integer course = 0;
    private Integer countStudent = 0;
    private String cipherGroup = "";
    private Integer countStream = 0;
    private Integer countGroup = 0;
    private Integer countSubgroup = 0;
    private Integer fullAmount = 0;
    private Integer lecture = 0;
    private Integer practicalExercises = 0;
    private Integer laboratory = 0;
    private Double individual = 0d;
    private Integer practice = 0;
    private Integer dipl = 0;
    private Integer exState = 0;
    private Integer offset = 0;
    private Integer exConsultation = 0;
    private Double exam = 0d;
    private Integer countGradStudent = 0;

    public void setData(int id, Object data) {
        switch (id) {
            case 1:
                nameDiscipline = (String) data;
                break;
            case 2:
                institute = (String) data;
                break;
            case 3:
                course = ((Double) data).intValue();
                break;
            case 4:
                countStudent = ((Double) data).intValue();
                break;
            case 5:
                cipherGroup = (String) data;
                break;
            case 6:
                countStream = ((Double) data).intValue();
                break;
            case 7:
                countGroup =((Double) data).intValue();
                break;
            case 8:
                countSubgroup = ((Double) data).intValue();
                break;
            case 9:
                fullAmount = ((Double) data).intValue();
                break;
            case 10:
                lecture = ((Double) data).intValue();
                break;
            case 11:
                practicalExercises = ((Double) data).intValue();
                break;
            case 12:
                laboratory = ((Double) data).intValue();
                break;
            case 13:
                individual = (Double) data;
                break;
            case 14:
                practice = ((Double) data).intValue();
                break;
            case 15:
                dipl = ((Double) data).intValue();
                break;
            case 16:
                exState = ((Double) data).intValue();
                break;
            case 17:
                offset = ((Double) data).intValue();
                break;
            case 18:
                exConsultation = ((Double) data).intValue();
                break;
            case 19:
                exam = (Double) data;
                break;
            case 20:
                countGradStudent = ((Double) data).intValue();
                break;
        }

    }

    public Object getData(int id) {
        Object result = null;

        switch (id) {
            case 1:
                result = nameDiscipline;
                break;
            case 2:
                result = institute;
                break;
            case 3:
                result = course;
                break;
            case 4:
                result = countStudent;
                break;
            case 5:
                result = cipherGroup;
                break;
            case 6:
                result = countStream;
                break;
            case 7:
                result = countGroup;
                break;
            case 8:
                result = countSubgroup;
                break;
            case 9:
                result = fullAmount;
                break;
            case 10:
                result = lecture;
                break;
            case 11:
                result = practicalExercises;
                break;
            case 12:
                result = laboratory;
                break;
            case 13:
                result = individual;
                break;
            case 14:
                result = practice;
                break;
            case 15:
                result = dipl;
                break;
            case 16:
                result = exState;
                break;
            case 17:
                result = offset;
                break;
            case 18:
                result = exConsultation;
                break;
            case 19:
                result = exam;
                break;
            case 20:
                result = countGradStudent;
                break;
        }
        return result;
    }
}
