package com.keosak.diploma.model.load;

import lombok.Data;

@Data
public class LoadCalculationPartGreenDto {
    private Integer lecturer;
    private Integer practice;
    private Integer laboratory;
    private Long individual;
    private Double consultation;
    private Integer offset;
    private Integer exConsultation;
    private Long exSemester;
    private Long controlPractice;
    private Double defPractice;
    private Integer controlDipl;
    private Integer gek;
    private Integer controlGradStudent;
    private Integer exState;
    private Double total;

    public void setData(LoadCalculationPartBlueDto data) {
        lecturer = considerLecturer(data);
        practice = considerPractice(data);
        laboratory = considerLaboratory(data);
        individual = considerIndividual(data);
        consultation = considerConsultation(data);
        offset = considerOffset(data);
        exConsultation = considerExConsultation(data);
        exSemester = considerExSemester(data);
        controlPractice = considerControlPractice(data);
        defPractice = considerDefPractice(data);
        controlDipl = considerControlDipl(data);
        gek = considerGek(data);
        controlGradStudent = considerControlGradStudent(data);
        exState = considerExState(data);
        total = considerTotal(data);
    }

    public Object getData(LoadCalculationPartBlueDto data, Integer id){
        Object result = null;

        switch (id) {
            case 21:
                result = considerLecturer(data);
                break;
            case 22:
                result = considerPractice(data);
                break;
            case 23:
                result = considerLaboratory(data);
                break;
            case 24:
                result = considerIndividual(data);
                break;
            case 25:
                result = considerConsultation(data);
                break;
            case 26:
                result = considerOffset(data);
                break;
            case 27:
                result = considerExConsultation(data);
                break;
            case 28:
                result = considerExSemester(data);
                break;
            case 29:
                result = considerControlPractice(data);
                break;
            case 30:
                result = considerDefPractice(data);
                break;
            case 31:
                result = considerControlDipl(data);
                break;
            case 32:
                result = considerGek(data);
                break;
            case 33:
                result = considerControlGradStudent(data);
                break;
            case 34:
                result = considerExState(data);
                break;
            case 35:
                result = considerTotal(data);
                break;
        }
        return result;
    }

    private Integer considerLecturer(LoadCalculationPartBlueDto data) {
        return data.getCountStream() * data.getLecture();
    }

    private Integer considerPractice(LoadCalculationPartBlueDto data) {
        return data.getCountGroup() * data.getPracticalExercises();
    }

    private Integer considerLaboratory(LoadCalculationPartBlueDto data) {
        return data.getCountSubgroup() * data.getLaboratory();
    }

    private Long considerIndividual(LoadCalculationPartBlueDto data) {
        return Math.round(data.getCountStudent() * data.getIndividual());
    }

    private Double considerConsultation(LoadCalculationPartBlueDto data) {
        return data.getCountGroup() * 0.04 * data.getFullAmount();
    }

    private Integer considerOffset(LoadCalculationPartBlueDto data) {
        return data.getCountGroup() * data.getOffset();
    }

    private Integer considerExConsultation(LoadCalculationPartBlueDto data) {
        return data.getCountGroup() * data.getExConsultation();
    }

    private Long considerExSemester(LoadCalculationPartBlueDto data) {
        return Math.round(data.getCountStudent() * data.getExam());
    }

    private Long considerControlPractice(LoadCalculationPartBlueDto data) {
        return Math.round(data.getCountStudent() * 0.25 * data.getPractice());
    }

    private Double considerDefPractice(LoadCalculationPartBlueDto data) {
        if (data.getPractice() > 1) {
            return data.getCountStudent() * 0.25;
        } else {
            return 0d;
        }
    }

    private Integer considerControlDipl(LoadCalculationPartBlueDto data) {
        return data.getCountStudent() * data.getDipl();
    }

    private Integer considerGek(LoadCalculationPartBlueDto data) {
        if (data.getExState() > 1) {
            return data.getCountStudent();
        } else {
            return 0;
        }
    }

    private Integer considerControlGradStudent(LoadCalculationPartBlueDto data) {
        return data.getCountGradStudent() * 25;
    }

    private Integer considerExState(LoadCalculationPartBlueDto data) {
        return data.getCountStudent() * data.getExState();
    }

    private Double considerTotal(LoadCalculationPartBlueDto data){
        return considerLecturer(data) + considerPractice(data) + considerLaboratory(data)
        + considerIndividual(data) + considerConsultation(data) + considerOffset(data)
        + considerExConsultation(data) + considerExSemester(data) + considerControlPractice(data)
        + considerDefPractice(data) + considerControlDipl(data) + considerGek(data)
        + considerControlGradStudent(data) + considerExState(data);
    }
}
