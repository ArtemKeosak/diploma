package com.keosak.diploma.model.load.util;

import com.keosak.diploma.model.load.LoadCalculationPartBlueDto;
import com.keosak.diploma.model.load.LoadCalculationPartGreenDto;
import lombok.Data;

@Data
public class LoadCalculationDto {
    private LoadCalculationPartBlueDto partBlueDto = new LoadCalculationPartBlueDto();
    private LoadCalculationPartGreenDto partGreenDto = new LoadCalculationPartGreenDto();
    public static final Integer FIRST_INDEX_PART_BLUE = 1;
    public static final Integer LAST_INDEX_PART_BLUE = 20;
    public static final Integer LAST_INDEX_PART_GREEN = 35;

    public LoadCalculationDto(LoadCalculationPartBlueDto partBlueDto, LoadCalculationPartGreenDto partGreenDto) {
        this.partBlueDto = partBlueDto;
        this.partGreenDto = partGreenDto;
    }

    public LoadCalculationDto() {

    }

    public void setBluePartById(int id, Object data){
        if (data == null){
            return;
        }
        if (id >= FIRST_INDEX_PART_BLUE && id <= LAST_INDEX_PART_BLUE){
            partBlueDto.setData(id, data);
        }
    }

    public void setGreenPart(LoadCalculationPartBlueDto partBlueDto){
        partGreenDto.setData(partBlueDto);
    }

    public Object getAllPart(int id){
        if (id >= FIRST_INDEX_PART_BLUE && id <= LAST_INDEX_PART_BLUE){
            return partBlueDto.getData(id);
        } else if (id > LAST_INDEX_PART_BLUE && id <= LAST_INDEX_PART_GREEN){
            return partGreenDto.getData(partBlueDto, id);
        } else {
            return null;
        }
    }

    public void setNumberSemester(Integer number) {
        partBlueDto.setNumberSemester(number);
    }

}
