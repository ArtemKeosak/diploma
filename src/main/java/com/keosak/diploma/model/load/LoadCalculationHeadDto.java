package com.keosak.diploma.model.load;

import com.keosak.diploma.model.load.util.LoadCalculationDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LoadCalculationHeadDto {

    private Long id;
    private Long idUser;
    private String year;
    private String department;
    private String institute;
    private List<LoadCalculationDto> loadCalculationDto = new ArrayList<>();
}
