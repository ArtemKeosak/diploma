package com.keosak.diploma.model;

import lombok.Data;

@Data
public class RecordDto {

    private String fullName;
    private String position;
    private Integer lecture;
    private Integer practice;
    private Integer laboratory;
    private Integer consultations;
    private Integer exConsultation;
    private Integer offset;
    private Integer exam;
    private Integer checkTest;
    private Integer coursework;
    private Integer dipl;
    private Integer dek;
    private Integer leadAsp;
    private Integer leadPractice;
    private Integer rgr;
    private Integer sum;

    public Object getData(int id){
        Object data = null;

        switch (id) {
            case 1:
                data = fullName;
                break;
            case 2:
                data = position;
                break;
            case 3:
                data = lecture;
                break;
            case 4:
                data = practice;
                break;
            case 5:
                data = laboratory;
                break;
            case 6:
                data = consultations;
                break;
            case 7:
                data = exConsultation;
                break;
            case 8:
                data = offset;
                break;
            case 9:
                data = exam;
                break;
            case 10:
                data = checkTest;
                break;
            case 11:
                data = coursework;
                break;
            case 12:
                data = dipl;
                break;
            case 13:
                data = dek;
                break;
            case 14:
                data = leadAsp;
                break;
            case 15:
                data = leadPractice;
                break;
            case 16:
                data = rgr;
                break;
            case 17:
                data = sum;
                break;
        }
        return data;
    }

    public RecordDto setSum() {
        sum = lecture + practice + laboratory + consultations  + exConsultation + offset
                + exam + checkTest + coursework + dipl + dek + leadAsp + leadPractice + rgr;
        return this;
    }

    public RecordDto setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public RecordDto setPosition(String position) {
        this.position = position;
        return this;
    }

    public RecordDto setLecture(Integer lecture) {
        this.lecture = lecture;
        return this;
    }

    public RecordDto setPractice(Integer practice) {
        this.practice = practice;
        return this;
    }

    public RecordDto setLaboratory(Integer laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    public RecordDto setConsultations(Integer consultations) {
        this.consultations = consultations;
        return this;
    }

    public RecordDto setExConsultation(Integer exConsultation) {
        this.exConsultation = exConsultation;
        return this;
    }

    public RecordDto setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public RecordDto setExam(Integer exam) {
        this.exam = exam;
        return this;
    }

    public RecordDto setCheckTest(Integer checkTest) {
        this.checkTest = checkTest;
        return this;
    }
    public RecordDto setCoursework(Integer coursework) {
        this.coursework = coursework;
        return this;
    }

    public RecordDto setDipl(Integer dipl) {
        this.dipl = dipl;
        return this;
    }

    public RecordDto setDek(Integer dek) {
        this.dek = dek;
        return this;
    }

    public RecordDto setLeadAsp(Integer leadAsp) {
        this.leadAsp = leadAsp;
        return this;
    }

    public RecordDto setLeadPractice(Integer leadPractice) {
        this.leadPractice = leadPractice;
        return this;
    }

    public RecordDto setRgr(Integer rgr) {
        this.rgr = rgr;
        return this;
    }
}
