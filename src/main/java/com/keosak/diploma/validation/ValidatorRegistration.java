package com.keosak.diploma.validation;

import com.keosak.diploma.model.error.ErrorRegDto;
import com.keosak.diploma.entity.user.User;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
public class ValidatorRegistration {

    public List<ErrorRegDto> validateUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("User is null");
        }
        List<ErrorRegDto> errors = new ArrayList<>();
        errors.add(validateUsername(user.getUsername()));
        errors.add(validatePassword(user.getPassword()));
        errors.add(validatePasswordConfirm(user.getPasswordConfirm(), user.getPassword()));
        errors.add(validateFullName(user.getFullName()));
        try {
            errors.add(validateStartDate(user.getStartContract()));
            errors.add(validateEndDate(user.getStartContract(), user.getEndContract()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        errors.add(validateShareRate(user.getShareRate()));
        errors.add(validateMonthWorked(user.getMonthsWorked()));

        errors.removeIf(Objects::isNull);
        return errors;
    }

    public ErrorRegDto validateUsername(String username) {
        String nameField = "errorUsername";
        ErrorRegDto result = null;
        if (username == null || username.length() < 5 || username.length() > 20) {
            result = new ErrorRegDto(nameField,
                    "Довжина \"username\" повинна бути від 5 до 20");
        }

        return result;
    }

    public ErrorRegDto validatePassword(String password) {
        String nameField = "errorPassword";
        ErrorRegDto result = null;
        if (password == null || password.length() < 5 || password.length() > 20) {
            result = new ErrorRegDto(nameField,
                    "Довжина \"password\" повинна бути від 5 до 20");
        }

        return result;
    }

    public ErrorRegDto validatePasswordConfirm(String passwordConfirm, String password) {
        String nameField = "errorPasswordConfirm";
        if (password == null) {
            return new ErrorRegDto("errorPassword", "Потрібно ввести password");
        }

        ErrorRegDto result = null;
        if (passwordConfirm == null || passwordConfirm.length() < 5 || passwordConfirm.length() > 20) {
            result = new ErrorRegDto(nameField,
                    "Довжина \"password confirm\" повинна бути від 5 до 20");
        } else if (!password.equals(passwordConfirm)) {
            result = new ErrorRegDto(nameField,
                    "Поля \"password\" і \"password confirm\" не співпадають");
        }

        return result;
    }

    public ErrorRegDto validateFullName(String fullName) {
        String nameField = "errorFullName";
        ErrorRegDto result = null;
        if (fullName == null || fullName.length() < 2 || fullName.length() > 50) {
            result = new ErrorRegDto(nameField,
                    "Довжина \"ПІБ\" повинна бути від 2 до 50");
        }

        return result;
    }

    public ErrorRegDto validateStartDate(String startDateStr) throws ParseException {
        String nameField = "errorStartDate";
        if (startDateStr == null || startDateStr.equals("")) {
            return new ErrorRegDto(nameField,
                    "Дата початку контракту повина бути не раніше 1980-01-01 та не пізніше 2100-12-31");
        }

        ErrorRegDto result = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = format.parse(startDateStr);
        Date minDate = format.parse("1980-01-01");
        Date maxDate = format.parse("2100-12-31");
        if (startDate.before(minDate) || startDate.after(maxDate)) {
            result = new ErrorRegDto(nameField,
                    "Дата початку контракту повина бути не раніше 1980-01-01 та не пізніше 2100-12-31");
        }

        return result;
    }

    public ErrorRegDto validateEndDate(String startDateStr, String endDateStr) throws ParseException {
        String nameField = "errorEndDate";
        if (endDateStr == null || endDateStr.equals("")) {
            return new ErrorRegDto(nameField,
                    "Дата кінця контракту повина бути не раніше 1980-01-01 та не пізніше 2100-12-31");
        } else if (startDateStr == null || startDateStr.equals("")) {
            return new ErrorRegDto("errorStartDate",
                    "Дата початку контракту повина бути не раніше 1980-01-01 та не пізніше 2100-12-31");
        }

        ErrorRegDto result = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = format.parse(startDateStr);
        Date endDate = format.parse(endDateStr);
        Date minDate = format.parse("1980-01-01");
        Date maxDate = format.parse("2100-12-31");
        if (endDate.before(minDate) || endDate.after(maxDate)) {
            result = new ErrorRegDto(nameField,
                    "Дата кінця контракту повина бути не раніше 1980-01-01 та не пізніше 2100-12-31");
        } else if (endDate.before(startDate)) {
            result = new ErrorRegDto(nameField,
                    "Дата кінця контракту повина бути пізніше дати початку контракту");
        }

        return result;
    }

    public ErrorRegDto validateShareRate(Float shareRate) {
        String nameField = "errorShareRate";
        ErrorRegDto result = null;
        if (shareRate == null || shareRate < 0 || shareRate > 5){
            result = new ErrorRegDto(nameField,
                    "Ставка повина бути від 0 до 5");
        } else if (shareRate.toString().split("\\.")[1].length() > 1){
            result = new ErrorRegDto(nameField,
                    "Кількість цифр після коми/точки повино бути не більше 1");
        }
        return result;
    }

    public ErrorRegDto validateMonthWorked(Integer monthWorked) {
        String nameField = "errorMonthWorked";
        ErrorRegDto result = null;
        if (monthWorked == null || monthWorked < 0 || monthWorked > 480){
            result = new ErrorRegDto(nameField,
                    "Кількість місяців повина бути від 0 до 580");
        }
        return result;
    }
}
