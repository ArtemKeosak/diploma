package com.keosak.diploma.validation;

import com.keosak.diploma.model.individual.IndividualPlanHeadDto;
import org.springframework.stereotype.Component;

@Component
public class ValidatorIndividualPlan extends BaseValidatorApache{

    public void validatePartsSize(IndividualPlanHeadDto headDto){
        if (headDto.getPart1().size() + headDto.getPart2().size() +headDto.getPart3().size()
                + headDto.getPart4().size() + headDto.getPart5().size() <= 0){
            throw new IllegalArgumentException("Файл порожній");
        }
    }
}
