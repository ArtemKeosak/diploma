package com.keosak.diploma.validation;

import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import com.keosak.diploma.model.load.LoadCalculationHeadDto;
import com.keosak.diploma.model.load.util.LoadCalculationDto;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ValidatorLoadCalculation extends BaseValidatorApache{

    public LoadCalculationHeadDto validateHeadDto(LoadCalculationHeadDto headDto){
        if (headDto.getYear() != null && headDto.getYear().length() > 100){
            headDto.setYear(headDto.getYear().substring(0, 100));
        }
        if (headDto.getDepartment() != null && headDto.getDepartment().length() > 100){
            headDto.setDepartment(headDto.getDepartment().substring(0, 100));
        }
        if (headDto.getInstitute() != null && headDto.getInstitute().length() > 100){
            headDto.setInstitute(headDto.getInstitute().substring(0, 100));
        }

        return headDto;
    }

    public List<LoadCalculationEntity> validateList(List<LoadCalculationEntity> list){
        if (list == null || list.size() < 1){
            throw new IllegalArgumentException("Load calculation with this id not found");
        } else {
            return list;
        }
    }

    public void validateNotEmptyBody(List<LoadCalculationDto> list){
        if (list.isEmpty()){
            throw new IllegalArgumentException("Файл порожный");
        }
    }

}
