package com.keosak.diploma.validation;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.jdbc.support.KeyHolder;

public abstract class BaseValidatorApache {

    public Object validateCell(Row hssfRow, int number){
        if (hssfRow.getCell(number) == null){
            return null;
        } else {
            Cell cell = hssfRow.getCell(number);

            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_FORMULA:
                case Cell.CELL_TYPE_NUMERIC:
                    return cell.getNumericCellValue();
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();
                case Cell.CELL_TYPE_BOOLEAN:
                    return cell.getBooleanCellValue();
                case Cell.CELL_TYPE_ERROR:
                    return cell.getErrorCellValue();
                default:
                    return null;
            }
        }
    }

    public Long validateId(KeyHolder keyHolder){
        try {
            if (keyHolder.getKey() == null) {
                throw new IllegalAccessException("Illegal data");
            } else {
                Long id = keyHolder.getKey().longValue();
                return validateId(id);
            }
        } catch (Exception e){
            throw new IllegalArgumentException("Illegal data");
        }

    }

    public Long validateId(Long id){
        if (id == null || id < 0){
            throw new IllegalArgumentException("Illegal id");
        } else {
            return id;
        }
    }

}
