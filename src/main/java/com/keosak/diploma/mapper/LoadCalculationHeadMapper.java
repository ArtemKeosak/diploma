package com.keosak.diploma.mapper;

import com.keosak.diploma.entity.load.LoadCalculationHeadEntity;
import com.keosak.diploma.model.load.LoadCalculationHeadDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface LoadCalculationHeadMapper {

    LoadCalculationHeadMapper INSTANCE = Mappers.getMapper(LoadCalculationHeadMapper.class);

    LoadCalculationHeadDto entityToDto(LoadCalculationHeadEntity entity);

    LoadCalculationHeadEntity dtoToEntity(LoadCalculationHeadDto entity);
}
