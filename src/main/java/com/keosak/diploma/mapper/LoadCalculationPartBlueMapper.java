package com.keosak.diploma.mapper;

import com.keosak.diploma.entity.load.LoadCalculationPartBlueEntity;
import com.keosak.diploma.model.load.LoadCalculationPartBlueDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface LoadCalculationPartBlueMapper {
    LoadCalculationPartBlueMapper INSTANCE = Mappers.getMapper(LoadCalculationPartBlueMapper.class);

    LoadCalculationPartBlueDto entityToDto(LoadCalculationPartBlueEntity entity);

    @Mapping(target = "idHead", ignore = true)
    LoadCalculationPartBlueEntity dtoToEntity(LoadCalculationPartBlueDto entity);
}
