package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart2Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart2Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart2Mapper {

    IndividualPlanPart2Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart2Mapper.class);

    IndividualPlanPart2Dto entityToDto(IndividualPlanPart2Entity entity);

    IndividualPlanPart2Entity dtoToEntity(IndividualPlanPart2Dto entity);
}
