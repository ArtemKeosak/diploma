package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanHeadEntity;
import com.keosak.diploma.model.individual.IndividualPlanHeadDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanHeadMapper {

    IndividualPlanHeadMapper INSTANCE = Mappers.getMapper(IndividualPlanHeadMapper.class);

    IndividualPlanHeadDto entityToDto(IndividualPlanHeadEntity entity);

    IndividualPlanHeadEntity dtoToEntity(IndividualPlanHeadDto entity);
}
