package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart5Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart5Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart5Mapper {

    IndividualPlanPart5Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart5Mapper.class);

    IndividualPlanPart5Dto entityToDto(IndividualPlanPart5Entity entity);

    IndividualPlanPart5Entity dtoToEntity(IndividualPlanPart5Dto entity);
}
