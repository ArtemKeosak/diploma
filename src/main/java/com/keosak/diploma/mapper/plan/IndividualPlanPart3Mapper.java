package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart3Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart3Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart3Mapper {

    IndividualPlanPart3Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart3Mapper.class);

    IndividualPlanPart3Dto entityToDto(IndividualPlanPart3Entity entity);

    IndividualPlanPart3Entity dtoToEntity(IndividualPlanPart3Dto entity);
}
