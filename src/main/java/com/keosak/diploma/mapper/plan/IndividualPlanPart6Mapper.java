package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart6Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart6Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart6Mapper {

    IndividualPlanPart6Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart6Mapper.class);

    IndividualPlanPart6Dto entityToDto(IndividualPlanPart6Entity entity);

    IndividualPlanPart6Entity dtoToEntity(IndividualPlanPart6Dto entity);
}
