package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanFrontPageEntity;
import com.keosak.diploma.entity.individual.IndividualPlanHeadEntity;
import com.keosak.diploma.model.individual.IndividualPlanFrontPageDto;
import com.keosak.diploma.model.individual.IndividualPlanHeadDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanFrontPageMapper {

    IndividualPlanFrontPageMapper INSTANCE = Mappers.getMapper(IndividualPlanFrontPageMapper.class);

    IndividualPlanFrontPageDto entityToDto(IndividualPlanFrontPageEntity entity);

    IndividualPlanFrontPageEntity dtoToEntity(IndividualPlanFrontPageDto entity);
}
