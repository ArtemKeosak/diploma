package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart4Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart4Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart4Mapper {

    IndividualPlanPart4Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart4Mapper.class);

    IndividualPlanPart4Dto entityToDto(IndividualPlanPart4Entity entity);

    IndividualPlanPart4Entity dtoToEntity(IndividualPlanPart4Dto entity);
}
