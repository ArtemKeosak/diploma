package com.keosak.diploma.mapper.plan;

import com.keosak.diploma.entity.individual.IndividualPlanPart1Entity;
import com.keosak.diploma.model.individual.IndividualPlanPart1Dto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualPlanPart1Mapper {

    IndividualPlanPart1Mapper INSTANCE = Mappers.getMapper(IndividualPlanPart1Mapper.class);

    IndividualPlanPart1Dto entityToDto(IndividualPlanPart1Entity entity);

    IndividualPlanPart1Entity dtoToEntity(IndividualPlanPart1Dto entity);
}
