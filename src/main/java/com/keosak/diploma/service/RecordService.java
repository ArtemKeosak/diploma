package com.keosak.diploma.service;

import com.keosak.diploma.entity.user.User;
import com.keosak.diploma.mapper.plan.IndividualPlanPart1Mapper;
import com.keosak.diploma.model.RecordDto;
import com.keosak.diploma.model.individual.IndividualPlanPart1Dto;
import com.keosak.diploma.repository.plan.IndividualPlanPart1Repository;
import com.keosak.diploma.repository.user.PositionRepository;
import com.keosak.diploma.service.apache.RecordApache;
import com.keosak.diploma.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RecordService {

    @Autowired
    UserService userService;
    @Autowired
    IndividualPlanPart1Repository part1Repository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    RecordApache apache;

    public InputStream getFile(String year) {
        return apache.modelToExel(getList(year));
    }

    public List<RecordDto> getList(String year) {
        List<RecordDto> listRecord = new ArrayList<>();
        for (User user : userService.allUsers()) {
            listRecord.add(
                    setRecord(part1Repository
                            .getPage1ByIdUserAndYear(user.getId(), year)
                            .stream()
                            .map(IndividualPlanPart1Mapper.INSTANCE::entityToDto)
                            .collect(Collectors.toList()), user));
        }

        return listRecord.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    private RecordDto setRecord(List<IndividualPlanPart1Dto> listPart1, User user) {
        RecordDto recordDto  = new RecordDto()
                .setLecture(listPart1
                        .stream()
                        .mapToInt(x -> x.getLecture())
                        .sum())
                .setPractice(listPart1
                        .stream()
                        .mapToInt(x -> x.getPractice())
                        .sum())
                .setLaboratory(listPart1
                        .stream()
                        .mapToInt(x -> x.getLaboratory())
                        .sum())
                .setConsultations(listPart1
                        .stream()
                        .mapToInt(x -> x.getConsultations())
                        .sum())
                .setExConsultation(listPart1
                        .stream()
                        .mapToInt(x -> x.getExConsultation())
                        .sum())
                .setOffset(listPart1
                        .stream()
                        .mapToInt(x -> x.getOffset())
                        .sum())
                .setExam(listPart1
                        .stream()
                        .mapToInt(x -> x.getExSemester())
                        .sum())
                .setCheckTest(listPart1
                        .stream()
                        .mapToInt(x -> x.getAudTest() + x.getIndTest())
                        .sum())
                .setCoursework(listPart1
                        .stream()
                        .mapToInt(x -> x.getCoursework())
                        .sum())
                .setDipl(listPart1
                        .stream()
                        .mapToInt(x -> x.getControlDipl())
                        .sum())
                .setDek(listPart1
                        .stream()
                        .mapToInt(x -> x.getExState())
                        .sum())
                .setLeadAsp(listPart1
                        .stream()
                        .mapToInt(x -> x.getControlAsp())
                        .sum())
                .setLeadAsp(listPart1
                        .stream()
                        .mapToInt(x -> x.getControlAsp())
                        .sum())
                .setLeadPractice(listPart1
                        .stream()
                        .mapToInt(x -> x.getControlPract())
                        .sum())
                .setRgr(listPart1
                        .stream()
                        .mapToInt(x -> x.getGraphicWork())
                        .sum())
                .setFullName(user.getFullName())
                .setPosition(user.getPosition().getShortName())
                .setSum();

        if (recordDto.getSum() == 0){
            return null;
        } else {
            return recordDto;
        }
    }
}
