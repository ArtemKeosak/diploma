package com.keosak.diploma.service;

import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import com.keosak.diploma.entity.user.User;
import com.keosak.diploma.mapper.LoadCalculationHeadMapper;
import com.keosak.diploma.mapper.LoadCalculationPartBlueMapper;
import com.keosak.diploma.mapper.plan.*;
import com.keosak.diploma.model.individual.IndividualPlanHeadDto;
import com.keosak.diploma.model.load.LoadCalculationHeadDto;
import com.keosak.diploma.model.load.util.LoadCalculationDto;
import com.keosak.diploma.repository.plan.*;
import com.keosak.diploma.service.apache.IndividualPlanApache;
import com.keosak.diploma.validation.ValidatorIndividualPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

//TODO
@Service
public class IndividualPlanService {

    @Autowired
    IndividualPlanApache apache;
    @Autowired
    ValidatorIndividualPlan validator;
    @Autowired
    IndividualPlanHeadRepository headRepository;
    @Autowired
    IndividualPlanFrontPageRepository frontPageRepository;

    @Autowired
    IndividualPlanPart1Repository part1Repository;
    @Autowired
    IndividualPlanPart2Repository part2Repository;
    @Autowired
    IndividualPlanPart3Repository part3Repository;
    @Autowired
    IndividualPlanPart4Repository part4Repository;
    @Autowired
    IndividualPlanPart5Repository part5Repository;
    @Autowired
    IndividualPlanPart6Repository part6Repository;

    public void addFile(MultipartFile file) {
        Long id = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        IndividualPlanHeadDto headDto = apache.exelToModel(file);
        validator.validatePartsSize(headDto);
        headDto.setIdUser(id);

        Long idHead = validator
                .validateId(headRepository
                        .addHead(IndividualPlanHeadMapper.INSTANCE.dtoToEntity(headDto)));

        frontPageRepository.addFrontPage(
                headDto.getFrontPages()
                        .stream()
                        .map(IndividualPlanFrontPageMapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));

        part1Repository.addPart1(
                headDto.getPart1()
                        .stream()
                        .map(IndividualPlanPart1Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
        part2Repository.addPart2(
                headDto.getPart2()
                        .stream()
                        .map(IndividualPlanPart2Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
        part3Repository.addPart3(
                headDto.getPart3()
                        .stream()
                        .map(IndividualPlanPart3Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
        part4Repository.addPart4(
                headDto.getPart4()
                        .stream()
                        .map(IndividualPlanPart4Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
        part5Repository.addPart5(
                headDto.getPart5()
                        .stream()
                        .map(IndividualPlanPart5Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
        part6Repository.addPart6(
                headDto.getPart6()
                        .stream()
                        .map(IndividualPlanPart6Mapper.INSTANCE::dtoToEntity)
                        .peek(x -> x.setIdHeader(idHead))
                        .collect(Collectors.toList()));
    }

    public IndividualPlanHeadDto findByIdUser(Long idUser) {
        idUser = validator.validateId(idUser);
        IndividualPlanHeadDto headDto = IndividualPlanHeadMapper
                .INSTANCE
                .entityToDto(headRepository.getByIdUser(idUser).get(0));

        Long idHead = headDto.getId();

        headDto.setFrontPages(frontPageRepository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanFrontPageMapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart1(part1Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart1Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart2(part2Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart2Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart3(part3Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart3Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart4(part4Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart4Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart5(part5Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart5Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));
        headDto.setPart6(part6Repository
                .getByIdHead(idHead)
                .stream()
                .map(IndividualPlanPart6Mapper.INSTANCE::entityToDto)
                .collect(Collectors.toList()));

        return headDto;
    }

    public InputStream getFile() {
        Long id = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return apache.modelToExel(findByIdUser(id));
    }

}
