package com.keosak.diploma.service;


import com.keosak.diploma.entity.load.LoadCalculationPartBlueEntity;
import com.keosak.diploma.entity.load.util.LoadCalculationEntity;
import com.keosak.diploma.model.load.LoadCalculationHeadDto;
import com.keosak.diploma.model.load.util.LoadCalculationDto;
import com.keosak.diploma.repository.load.LoadCalculationHeadRepository;
import com.keosak.diploma.repository.load.LoadCalculationPartBlueRepository;
import com.keosak.diploma.service.apache.LoadCalculationApache;
import com.keosak.diploma.mapper.LoadCalculationHeadMapper;
import com.keosak.diploma.mapper.LoadCalculationPartBlueMapper;
import com.keosak.diploma.validation.ValidatorLoadCalculation;
import com.keosak.diploma.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

//TODO
@Service
public class LoadCalculationService {
    @Autowired
    LoadCalculationApache apache;
    @Autowired
    LoadCalculationPartBlueRepository partBlueRepository;
    @Autowired
    LoadCalculationHeadRepository headRepository;
    @Autowired
    ValidatorLoadCalculation validator;

    public LoadCalculationHeadDto addFile(MultipartFile file) {
        Long idUser = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        LoadCalculationHeadDto headDto = validator.validateHeadDto(apache.exelToModel(file));
        validator.validateNotEmptyBody(headDto.getLoadCalculationDto());
        headDto.setIdUser(idUser);

        Long idHead = validator.validateId(headRepository.addHead(
                LoadCalculationHeadMapper.INSTANCE.dtoToEntity(headDto)));

        List<LoadCalculationPartBlueEntity> empList = new ArrayList<>();
        for (LoadCalculationDto dto : headDto.getLoadCalculationDto()) {
            LoadCalculationPartBlueEntity entity = LoadCalculationPartBlueMapper
                    .INSTANCE.dtoToEntity(dto.getPartBlueDto());
            entity.setIdHead(idHead);
            empList.add(entity);
        }

        partBlueRepository.addAllPartBlue(empList);

        //System.out.println(apache.modelToExel(headDto));
        return headDto;
    }

    public LoadCalculationHeadDto findByIdUser(Long idUser){
        idUser = validator.validateId(idUser);
        List<LoadCalculationEntity> list = validator.validateList(headRepository.findHeadByIdUser(idUser));
        LoadCalculationHeadDto headDto = LoadCalculationHeadMapper
                .INSTANCE.entityToDto(list.get(0).getHeadEntity());

        for (LoadCalculationEntity entity: list){
            LoadCalculationDto loadCalculationDto = new LoadCalculationDto();

            loadCalculationDto.setPartBlueDto(LoadCalculationPartBlueMapper
                    .INSTANCE.entityToDto(entity.getPartBlueEntity()));
            loadCalculationDto.setGreenPart(loadCalculationDto.getPartBlueDto());

            headDto.getLoadCalculationDto().add(loadCalculationDto);
        }

        return headDto;
    }

    public InputStream getFile(){
        Long id = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return apache.modelToExel(findByIdUser(id));
    }
}
