package com.keosak.diploma.service.user;

import com.keosak.diploma.entity.user.Role;
import com.keosak.diploma.entity.user.User;
import com.keosak.diploma.repository.user.PositionRepository;
import com.keosak.diploma.repository.user.RoleRepository;
import com.keosak.diploma.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
public class UserService implements UserDetailsService {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByUsername(user.getUsername());

        if (userFromDB != null) {
            return false;
        }

        user.setPosition(positionRepository.getById(user.getIdPosition()));
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    public List<User> usergtList(Long idMin) {
        return em.createQuery("SELECT u FROM User u WHERE u.id > :paramId", User.class)
                .setParameter("paramId", idMin).getResultList();
    }

    public void addSuperUser(){
        if (userRepository.findAll().isEmpty()) {
            User user = new User();
            user.setUsername("admin");
            user.setPassword("11111");
            user.setFullName("Admin");
            user.setStartContract("2001-01-01");
            user.setEndContract("2002-02-02");
            user.setShareRate(2F);
            user.setMonthsWorked(12);
            user.setPosition(positionRepository.getById(2L));

            user.setRoles(Set.of(new Role(1L, "ROLE_USER"), new Role(2L, "ROLE_ADMIN")));
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        }

    }

    public void setRole(){
        if (roleRepository.findAll().isEmpty()){
            List<Role> roles = Arrays.asList(
                    new Role(1L, "ROLE_USER"), new Role(2L, "ROLE_ADMIN"));
            roleRepository.saveAll(roles);
        }
    }
}
