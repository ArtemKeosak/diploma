package com.keosak.diploma.service.user;

import com.keosak.diploma.model.error.ErrorRegDto;
import com.keosak.diploma.validation.ValidatorRegistration;
import com.keosak.diploma.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationService {
    @Autowired
    UserService userService;
    @Autowired
    ValidatorRegistration validator;

    public List<ErrorRegDto> registration(User user){
        List<ErrorRegDto> errors = validator.validateUser(user);

        if (errors.isEmpty()) {
            if (!userService.saveUser(user)) {
                errors.add(new ErrorRegDto("errorUsername",
                        "Користувач з таким \"username\" вже існує"));
            }
        }
        return errors;
    }


}
