package com.keosak.diploma.service.user;

import com.keosak.diploma.entity.user.Position;
import com.keosak.diploma.repository.user.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PositionService {

    @Autowired
    PositionRepository repository;

    public void setPosition(){
        if (repository.findAll().isEmpty()){
            List<Position> positions = Arrays.asList(
                    new Position(1L, "Доцент", "д."),
                    new Position(2L, "Професор", "п."),
                    new Position(3L, "Асистент", "а."),
                    new Position(4L, "Ст. викладач", "ст."));
            repository.saveAll(positions);
        }
    }
}
