package com.keosak.diploma.service.apache.util;

public class ApacheConstant {

    public static final int[] LOAD_CALCULATION_ID_ROW_START = new int[]{9, 5};

    public static final int IND_PLAN_FRONT_PAGE = 30;

    public static final int[][] IND_PLAN_PART1 = new int[][]{
            {1, 5, 15},
            {1, 16, 25},
            {1, 28, 37},
            {1, 18, 30},
            {1, 40, 48}
    };

    public static final int[][] IND_PLAN_PART2 = new int[][]{
            {6, 3, 21},
            {6, 25, 43},
            {7, 2, 14},
            {7, 18, 30},
            {7, 34, 46}
    };

    public static final int[][] IND_PLAN_PART3 = new int[][]{
            {8, 4, 22},
            {8, 26, 44},
            {9, 3, 15},
            {9, 19, 31},
            {9, 35, 47}
    };

    public static final int[][] IND_PLAN_PART4 = new int[][]{
            {10, 4, 22},
            {10, 26, 44},
            {11, 3, 15},
            {11, 19, 31},
            {11, 35, 47}
    };

    public static final int[][] IND_PLAN_PART5 = new int[][]{
            {12, 4, 48}
    };

    public static final int[][] IND_PLAN_PART6 = new int[][]{
            {13, 4, 40},
            {14, 2, 20},
            {14, 24, 49}
    };

    public static final int[][] IND_PLAN_PART1_CELL = new int[][]{
            {2, 2},
            {3, 3},
            {4, 4},
            {5, 5},
            {6, 6},
            {7, 7, 8},
            {8, 9, 10},
            {9, 11, 12},
            {10, 13, 14},
            {11, 15, 16},
            {12, 17, 18},
            {13, 19, 20},
            {14, 21, 22},
            {15, 23, 24},
            {16, 25, 26},
            {17, 27, 28},
            {18, 29, 30},
            {19, 31, 32},
            {20, 33, 34},
            {21, 35, 36},
            {22, 37, 38},
            {23, 39, 40},
            {24, 41, 42}
    };

    public static final int RECORD_START_ROW = 2;
    public static final int RECORD_START_CELL = 1;
    public static final int RECORD_START_FINISH_CELL = 17;
}
