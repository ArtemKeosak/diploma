package com.keosak.diploma.service.apache;

import com.keosak.diploma.model.RecordDto;
import com.keosak.diploma.model.individual.IndividualPlanHeadDto;
import com.keosak.diploma.service.apache.util.ApacheConstant;
import com.keosak.diploma.validation.ValidatorRecord;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;

@Component
public class RecordApache {

    @Autowired
    ValidatorRecord validator;
    private final String FILE_ROOT_NAME = "src/main/resources/exel/Record.xls";

    public InputStream modelToExel(List<RecordDto> listRecord) {
        File file;
        try {
            file = File.createTempFile("temp-", ".xls");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
        String fileName = file.toPath().toString();

        try (FileInputStream fin = new FileInputStream(FILE_ROOT_NAME);
             FileOutputStream fos
                     = new FileOutputStream(fileName)) {

            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);
            fos.write(buffer, 0, buffer.length);

        } catch (IOException ex) {
            throw new IllegalArgumentException("Error with file");
        }

        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(fileName))) {
            HSSFWorkbook wb = new HSSFWorkbook(fis);
            if (listRecord.size() > 0) {
                setData(wb.getSheetAt(0), listRecord);
            }

            try (BufferedOutputStream fio = new BufferedOutputStream(new FileOutputStream(fileName))) {
                wb.write(fio);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = new FileInputStream(file);
            file.deleteOnExit();
            return inputStream;
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
    }

    private void setData(HSSFSheet hssfSheet, List<RecordDto> listRecord) {
        int rowNum;
        for (rowNum = 0; rowNum < listRecord.size(); rowNum++) {
            for (int cellNum = ApacheConstant.RECORD_START_CELL;
                 cellNum <= ApacheConstant.RECORD_START_FINISH_CELL; cellNum++) {
                setCell(listRecord.get(rowNum).getData(cellNum), hssfSheet,
                        rowNum + ApacheConstant.RECORD_START_ROW, cellNum);
            }
        }
        setSum(hssfSheet, listRecord, rowNum + 2);
    }

    private void setSum(HSSFSheet hssfSheet, List<RecordDto> listRecord, int rowNum) {
        RecordDto result = new RecordDto()
                .setFullName("Всього")
                .setLecture(listRecord
                        .stream()
                        .mapToInt(x -> x.getLecture())
                        .sum())
                .setLecture(listRecord
                        .stream()
                        .mapToInt(x -> x.getLecture())
                        .sum())
                .setPractice(listRecord
                        .stream()
                        .mapToInt(x -> x.getPractice())
                        .sum())
                .setLaboratory(listRecord
                        .stream()
                        .mapToInt(x -> x.getLaboratory())
                        .sum())
                .setConsultations(listRecord
                        .stream()
                        .mapToInt(x -> x.getConsultations())
                        .sum())
                .setExConsultation(listRecord
                        .stream()
                        .mapToInt(x -> x.getExConsultation())
                        .sum())
                .setOffset(listRecord
                        .stream()
                        .mapToInt(x -> x.getOffset())
                        .sum())
                .setExam(listRecord
                        .stream()
                        .mapToInt(x -> x.getExam())
                        .sum())
                .setCheckTest(listRecord
                        .stream()
                        .mapToInt(x -> x.getCheckTest())
                        .sum())
                .setCoursework(listRecord
                        .stream()
                        .mapToInt(x -> x.getCoursework())
                        .sum())
                .setDipl(listRecord
                        .stream()
                        .mapToInt(x -> x.getDipl())
                        .sum())
                .setDek(listRecord
                        .stream()
                        .mapToInt(x -> x.getDek())
                        .sum())
                .setLeadAsp(listRecord
                        .stream()
                        .mapToInt(x -> x.getLeadAsp())
                        .sum())
                .setLeadPractice(listRecord
                        .stream()
                        .mapToInt(x -> x.getLeadPractice())
                        .sum())
                .setRgr(listRecord
                        .stream()
                        .mapToInt(x -> x.getRgr())
                        .sum())
                .setSum();


        for (int cellNum = ApacheConstant.RECORD_START_CELL;
             cellNum <= ApacheConstant.RECORD_START_FINISH_CELL; cellNum++) {
            setCell(result.getData(cellNum), hssfSheet,
                    rowNum + ApacheConstant.RECORD_START_ROW, cellNum);
        }

    }


    private void setCell(Object data, HSSFSheet hssfSheet, int rowNum, int cellNum) {
        if (hssfSheet.getRow(rowNum) == null) {
            hssfSheet.createRow(rowNum);
        }

        if (hssfSheet.getRow(rowNum).getCell(cellNum) == null) {
            hssfSheet.getRow(rowNum).createCell(cellNum);
        }

        if (data instanceof String) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((String) data);
        } else if (data instanceof Integer) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Integer) data);
        } else if (data instanceof Long) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Long) data);
        } else if (data instanceof Double) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Double) data);
        }
    }
}
