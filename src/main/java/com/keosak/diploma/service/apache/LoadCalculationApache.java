package com.keosak.diploma.service.apache;

import com.keosak.diploma.model.load.LoadCalculationHeadDto;
import com.keosak.diploma.model.load.LoadCalculationPartBlueDto;
import com.keosak.diploma.model.load.util.LoadCalculationDto;
import com.keosak.diploma.service.apache.util.ApacheConstant;
import com.keosak.diploma.validation.ValidatorLoadCalculation;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoadCalculationApache {
    @Autowired
    ValidatorLoadCalculation validator;
    private final String FILE_ROOT_NAME = "src/main/resources/exel/Load.xls";

    public LoadCalculationHeadDto exelToModel(MultipartFile file) {
        LoadCalculationHeadDto headDBO = new LoadCalculationHeadDto();
        try (InputStream is = file.getInputStream()) {
            HSSFWorkbook wb = new HSSFWorkbook(is);
            setHeadData(wb, headDBO);
            for (int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
                HSSFSheet hssfSheet = wb.getSheetAt(sheetNum); // Получить первую страницу листа
                if (hssfSheet == null) {
                    continue;
                }

                int idStart = ApacheConstant.LOAD_CALCULATION_ID_ROW_START[sheetNum];
                for (int rowNum = idStart; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                    HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                    if (hssfRow == null || hssfRow.getCell(1) == null
                            || hssfRow.getCell(1).getCellType() == Cell.CELL_TYPE_BLANK) {
                        continue;
                    }

                    headDBO.getLoadCalculationDto().add(setAllPart(hssfRow, sheetNum + 1));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return headDBO;
    }

    public InputStream modelToExel(LoadCalculationHeadDto headDto) {
        File file;
        try {
            file = File.createTempFile("temp-", ".xls");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
        String fileName = file.toPath().toString();

        try (FileInputStream fin = new FileInputStream(FILE_ROOT_NAME);
             FileOutputStream fos
                     = new FileOutputStream(fileName)) {

            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);
            fos.write(buffer, 0, buffer.length);

        } catch (IOException ex) {
            throw new IllegalArgumentException("Error with file");
        }


        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(fileName))) {
            HSSFWorkbook wb = new HSSFWorkbook(fis);

            int countSkipElementList = 0;
            for (int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
                HSSFSheet hssfSheet = wb.getSheetAt(sheetNum);
                if (hssfSheet == null) {
                    continue;
                }

                int[] arrayLengthRow = createArrayLength(headDto);

                int idStart = ApacheConstant.LOAD_CALCULATION_ID_ROW_START[sheetNum];
                if (arrayLengthRow[sheetNum] > 0) {
                    createNewRow(hssfSheet, idStart + arrayLengthRow[sheetNum]);

                    for (int rowNum = idStart; rowNum <= idStart + arrayLengthRow[sheetNum] - 1; rowNum++) {
                        HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                        setCellValue(hssfRow, headDto.getLoadCalculationDto()
                                .get(countSkipElementList + rowNum - idStart));
                    }

                    HSSFRow hssfRow = hssfSheet.createRow(idStart + arrayLengthRow[sheetNum]  + 1);
                    setCellValue(hssfRow, setSum(headDto.getLoadCalculationDto(), sheetNum+1));
                    countSkipElementList += arrayLengthRow[sheetNum];
                }
            }

            try (BufferedOutputStream fio = new BufferedOutputStream(new FileOutputStream(fileName))) {
                wb.write(fio);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = new FileInputStream(file);
            file.deleteOnExit();
            return inputStream;
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
    }


    private LoadCalculationDto setAllPart(Row hssfRow, int numberSemester) {
        LoadCalculationDto loadCalculationDto = new LoadCalculationDto();
        loadCalculationDto.setNumberSemester(numberSemester);
        for (int i = LoadCalculationDto.FIRST_INDEX_PART_BLUE; i <= LoadCalculationDto.LAST_INDEX_PART_BLUE; i++) {
            loadCalculationDto.setBluePartById(i, validator.validateCell(hssfRow, i));
        }
        loadCalculationDto.setGreenPart(loadCalculationDto.getPartBlueDto());
        return loadCalculationDto;
    }

    private void createNewRow(HSSFSheet hssfSheet, int length) {
        for (int maxRow = hssfSheet.getLastRowNum() + 1; maxRow <= length; maxRow++) {
            hssfSheet.createRow(maxRow);
            hssfSheet.getRow(maxRow).createCell(1);

        }
    }

    private int[] createArrayLength(LoadCalculationHeadDto headDto) {
        return new int[]{
                (int) headDto
                        .getLoadCalculationDto()
                        .stream()
                        .filter(x -> x.getPartBlueDto().getNumberSemester() == 1)
                        .count(),
                (int) headDto
                        .getLoadCalculationDto()
                        .stream()
                        .filter(x -> x.getPartBlueDto().getNumberSemester() == 2)
                        .count()};
    }

    private void setCellValue(HSSFRow hssfRow, LoadCalculationDto dto) {
        for (int cellIndex = LoadCalculationDto.FIRST_INDEX_PART_BLUE;
             cellIndex <= LoadCalculationDto.LAST_INDEX_PART_GREEN; cellIndex++) {
            Cell cell = hssfRow.createCell(cellIndex);
            Object data = dto.getAllPart(cellIndex);

            if (data instanceof String) {
                cell.setCellValue((String) data);
            } else if (data instanceof Integer) {
                cell.setCellValue((Integer) data);
            } else if (data instanceof Long) {
                cell.setCellValue((Long) data);
            } else if (data instanceof Double) {
                cell.setCellValue((Double) data);
            }

        }
    }

    private void setHeadData(HSSFWorkbook wb, LoadCalculationHeadDto headDto) {
        HSSFSheet hssfSheet = wb.getSheetAt(0);
        if (hssfSheet != null) {
            if (hssfSheet.getRow(1) != null && hssfSheet.getRow(1).getCell(4) != null) {
                headDto.setYear(hssfSheet.getRow(1).getCell(4).getStringCellValue());
            }
            if (hssfSheet.getRow(2) != null && hssfSheet.getRow(2).getCell(1) != null) {
                headDto.setDepartment(hssfSheet.getRow(2).getCell(1).getStringCellValue());
            }

            if (hssfSheet.getRow(4) != null && hssfSheet.getRow(4).getCell(1) != null) {
                headDto.setInstitute(hssfSheet.getRow(4).getCell(1).getStringCellValue());
            }
        }
    }

    private LoadCalculationDto setSum(List<LoadCalculationDto> listRecord, int numberSemester) {
        listRecord = listRecord
                .stream()
                .filter(x -> x.getPartBlueDto().getNumberSemester() == numberSemester)
                .collect(Collectors.toList());

        LoadCalculationPartBlueDto sumBlue = new LoadCalculationPartBlueDto();
        sumBlue.setNameDiscipline("Всього");
        sumBlue.setCountStudent(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getCountStudent())
                .sum());
        sumBlue.setCountStream(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getCountStream())
                .sum());
        sumBlue.setCountGroup(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getCountGroup())
                .sum());
        sumBlue.setCountSubgroup(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getCountSubgroup())
                .sum());
        sumBlue.setFullAmount(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getFullAmount())
                .sum());
        sumBlue.setLecture(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getLecture())
                .sum());
        sumBlue.setPracticalExercises(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getPracticalExercises())
                .sum());
        sumBlue.setLaboratory(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getLaboratory())
                .sum());
        sumBlue.setIndividual(listRecord
                .stream()
                .mapToDouble(x -> x.getPartBlueDto().getIndividual())
                .sum());
        sumBlue.setPractice(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getPractice())
                .sum());
        sumBlue.setDipl(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getDipl())
                .sum());
        sumBlue.setExState(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getExState())
                .sum());
        sumBlue.setOffset(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getOffset())
                .sum());
        sumBlue.setExConsultation(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getExConsultation())
                .sum());
        sumBlue.setExam(listRecord
                .stream()
                .mapToDouble(x -> x.getPartBlueDto().getExam())
                .sum());
        sumBlue.setCountGradStudent(listRecord
                .stream()
                .mapToInt(x -> x.getPartBlueDto().getCountGradStudent())
                .sum());

        LoadCalculationDto result = new LoadCalculationDto();
        result.setPartBlueDto(sumBlue);
        result.setGreenPart(sumBlue);
        return result;
    }
}
