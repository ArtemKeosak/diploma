package com.keosak.diploma.service.apache;

import com.keosak.diploma.model.individual.*;
import com.keosak.diploma.service.apache.util.ApacheConstant;
import com.keosak.diploma.validation.ValidatorIndividualPlan;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class IndividualPlanApache {

    @Autowired
    ValidatorIndividualPlan validator;
    private final String FILE_ROOT_NAME = "src/main/resources/exel/Plan.xls";

    public IndividualPlanHeadDto exelToModel(MultipartFile file) {
        IndividualPlanHeadDto header = new IndividualPlanHeadDto();
        try (InputStream is = file.getInputStream()) {
            HSSFWorkbook wb = new HSSFWorkbook(is);
            header = getHeadData(wb);
            for (int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
                HSSFSheet hssfSheet = wb.getSheetAt(sheetNum); // Получить первую страницу листа
                if (hssfSheet == null) {
                    continue;
                }

                switch (sheetNum) {
                    case 0:
                        header.getFrontPages().addAll(getFrontPage(hssfSheet));
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        header.getPart1().addAll(getPart1(hssfSheet, sheetNum));
                        break;
                    case 6:
                    case 7:
                        header.getPart2().addAll(getPart2(hssfSheet, sheetNum));
                        break;
                    case 8:
                    case 9:
                        header.getPart3().addAll(getPart3(hssfSheet, sheetNum));
                        break;
                    case 10:
                    case 11:
                        header.getPart4().addAll(getPart4(hssfSheet, sheetNum));
                        break;
                    case 12:
                        header.getPart5().addAll(getPart5(hssfSheet, sheetNum));
                        break;
                    case 13:
                    case 14:
                        header.getPart6().addAll(getPart6(hssfSheet, sheetNum));
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return header;
    }

    public InputStream modelToExel(IndividualPlanHeadDto headDto) {
        File file;
        try {
            file = File.createTempFile("temp-", ".xls");
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
        String fileName = file.toPath().toString();

        try (FileInputStream fin = new FileInputStream(FILE_ROOT_NAME);
             FileOutputStream fos
                     = new FileOutputStream(fileName)) {

            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);
            fos.write(buffer, 0, buffer.length);

        } catch (IOException ex) {
            throw new IllegalArgumentException("Error with file");
        }

        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(fileName))) {
            HSSFWorkbook wb = new HSSFWorkbook(fis);
            setHeadData(wb, headDto);

            for (int sheetNum = 0; sheetNum < wb.getNumberOfSheets(); sheetNum++) {
                HSSFSheet hssfSheet = wb.getSheetAt(sheetNum); // Получить первую страницу листа
                if (hssfSheet == null) {
                    continue;
                }

                switch (sheetNum) {
                    case 0:
                        setFrontPage(hssfSheet, headDto.getFrontPages());
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        setPart1(hssfSheet, headDto.getPart1(), sheetNum);
                        break;
                    case 6:
                    case 7:
                        setPart2(hssfSheet, headDto.getPart2(), sheetNum);
                        break;
                    case 8:
                    case 9:
                        setPart3(hssfSheet, headDto.getPart3(), sheetNum);
                        break;
                    case 10:
                    case 11:
                        setPart4(hssfSheet, headDto.getPart4(), sheetNum);
                        break;
                    case 12:
                        setPart5(hssfSheet, headDto.getPart5(), sheetNum);
                        break;
                    case 13:
                    case 14:
                        setPart6(hssfSheet, headDto.getPart6(), sheetNum);
                        break;
                }
            }

            try (BufferedOutputStream fio = new BufferedOutputStream(new FileOutputStream(fileName))) {
                wb.write(fio);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = new FileInputStream(file);
            file.deleteOnExit();
            return inputStream;
        } catch (IOException e) {
            throw new IllegalArgumentException("Error with file");
        }
    }

    private IndividualPlanHeadDto getHeadData(HSSFWorkbook wb) {
        IndividualPlanHeadDto header = new IndividualPlanHeadDto();
        HSSFSheet hssfSheet = wb.getSheetAt(0);
        if (hssfSheet != null) {
            if (hssfSheet.getRow(0) != null && hssfSheet.getRow(0).getCell(2) != null) {
                header.setHeaderName(hssfSheet.getRow(0).getCell(2).getStringCellValue());
            }
            if (hssfSheet.getRow(3) != null && hssfSheet.getRow(3).getCell(0) != null) {
                header.setUniversity(hssfSheet.getRow(3).getCell(0).getStringCellValue());
            }
            if (hssfSheet.getRow(6) != null && hssfSheet.getRow(6).getCell(0) != null) {
                header.setInstitute(hssfSheet.getRow(6).getCell(0).getStringCellValue());
            }
            if (hssfSheet.getRow(8) != null && hssfSheet.getRow(8).getCell(0) != null) {
                header.setDepartment(hssfSheet.getRow(8).getCell(0).getStringCellValue());
            }
            if (hssfSheet.getRow(23) != null && hssfSheet.getRow(23).getCell(0) != null) {
                header.setFullName(hssfSheet.getRow(23).getCell(0).getStringCellValue());
            }
        }

        return header;
    }

    private void setHeadData(HSSFWorkbook wb, IndividualPlanHeadDto headDto) {
        HSSFSheet hssfSheet = wb.getSheetAt(0);
        hssfSheet.getRow(0).getCell(2).setCellValue(headDto.getHeaderName());
        hssfSheet.getRow(3).getCell(0).setCellValue(headDto.getUniversity());
        hssfSheet.getRow(6).getCell(0).setCellValue(headDto.getInstitute());
        hssfSheet.getRow(8).getCell(0).setCellValue(headDto.getDepartment());
        hssfSheet.getRow(23).getCell(0).setCellValue(headDto.getFullName());
    }

    private List<IndividualPlanFrontPageDto> getFrontPage(HSSFSheet hssfSheet) {
        List<IndividualPlanFrontPageDto> result = new ArrayList<>();

        int countRow = 1;
        for (int rowNum = ApacheConstant.IND_PLAN_FRONT_PAGE;
             rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
            HSSFRow hssfRow = hssfSheet.getRow(rowNum);
            if (hssfRow == null || hssfRow.getCell(0) == null
                    || hssfRow.getCell(0).getCellType() == Cell.CELL_TYPE_BLANK) {
                continue;
            }

            IndividualPlanFrontPageDto frontPage = new IndividualPlanFrontPageDto();
            frontPage.setPage(countRow);
            countRow++;
            for (int i = 0; i <= 5; i++) {
                frontPage.setData(i, validator.validateCell(hssfRow, i));
            }
            result.add(frontPage);
        }

        return result;
    }

    private void setFrontPage(HSSFSheet hssfSheet, List<IndividualPlanFrontPageDto> listFrontPage) {
        for (int rowNum = 0; rowNum < Math.min(listFrontPage.size(), 5); rowNum++) {
            for (int cellNum = 0; cellNum <= 5; cellNum++) {
                setCell(listFrontPage.get(rowNum).getData(cellNum), hssfSheet,
                        rowNum + ApacheConstant.IND_PLAN_FRONT_PAGE, cellNum);
            }
        }
    }

    private List<IndividualPlanPart1Dto> getPart1(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart1Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART1.length; i++) {

            for (int rowNum = ApacheConstant.IND_PLAN_PART1[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART1[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(6) == null
                        || hssfRow.getCell(6).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart1Dto part1 = new IndividualPlanPart1Dto();
                part1.setPage(sheetNum);
                part1.setPartPage(rowNum);
                for (int j = 0; j < ApacheConstant.IND_PLAN_PART1_CELL.length; j++) {
                    part1.setData(ApacheConstant.IND_PLAN_PART1_CELL[j][0],
                            validator.validateCell(hssfRow, ApacheConstant.IND_PLAN_PART1_CELL[j][1]));
                }
                result.add(part1);
            }
        }

        return result;
    }

    private void setPart1(HSSFSheet hssfSheet, List<IndividualPlanPart1Dto> listPart1, int sheetNum) {
        for (IndividualPlanPart1Dto part1Dto : listPart1) {
            if (part1Dto.getPage() == sheetNum) {
                for (int cellNum = 0; cellNum < ApacheConstant.IND_PLAN_PART1_CELL.length; cellNum++) {
                    for (int i = 1; i < ApacheConstant.IND_PLAN_PART1_CELL[cellNum].length; i++) {
                        setCell(part1Dto.getData(ApacheConstant.IND_PLAN_PART1_CELL[cellNum][0]),
                                hssfSheet, part1Dto.getPartPage(), ApacheConstant.IND_PLAN_PART1_CELL[cellNum][i]);
                    }
                }
            }
        }
    }

    private List<IndividualPlanPart2Dto> getPart2(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart2Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART2.length; i++) {
            if (ApacheConstant.IND_PLAN_PART2[i][0] != sheetNum)
                continue;

            for (int rowNum = ApacheConstant.IND_PLAN_PART2[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART2[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(1) == null
                        || hssfRow.getCell(1).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart2Dto part2 = new IndividualPlanPart2Dto();
                part2.setPage(sheetNum);
                part2.setPartPage(rowNum);
                for (int j = 1; j <= 4; j++) {
                    part2.setData(j, validator.validateCell(hssfRow, j));
                }
                result.add(part2);
            }
        }

        return result;
    }

    private void setPart2(HSSFSheet hssfSheet, List<IndividualPlanPart2Dto> listPart2, int sheetNum) {
        for (IndividualPlanPart2Dto part2Dto : listPart2) {
            if (part2Dto.getPage() == sheetNum) {
                for (int cellNum = 1; cellNum <= 4; cellNum++) {
                    setCell(part2Dto.getData(cellNum), hssfSheet, part2Dto.getPartPage(), cellNum);
                }
            }
        }
    }

    private List<IndividualPlanPart3Dto> getPart3(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart3Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART3.length; i++) {
            if (ApacheConstant.IND_PLAN_PART3[i][0] != sheetNum)
                continue;

            for (int rowNum = ApacheConstant.IND_PLAN_PART3[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART3[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(1) == null
                        || hssfRow.getCell(1).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart3Dto part3 = new IndividualPlanPart3Dto();
                part3.setPage(sheetNum);
                part3.setPartPage(rowNum);
                for (int j = 1; j <= 4; j++) {
                    part3.setData(j, validator.validateCell(hssfRow, j));
                }
                result.add(part3);
            }
        }

        return result;
    }

    private void setPart3(HSSFSheet hssfSheet, List<IndividualPlanPart3Dto> listPart3, int sheetNum) {
        for (IndividualPlanPart3Dto part3Dto : listPart3) {
            if (part3Dto.getPage() == sheetNum) {
                for (int cellNum = 1; cellNum <= 4; cellNum++) {
                    setCell(part3Dto.getData(cellNum), hssfSheet, part3Dto.getPartPage(), cellNum);
                }
            }
        }
    }

    private List<IndividualPlanPart4Dto> getPart4(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart4Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART4.length; i++) {
            if (ApacheConstant.IND_PLAN_PART4[i][0] != sheetNum)
                continue;

            for (int rowNum = ApacheConstant.IND_PLAN_PART4[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART4[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(1) == null
                        || hssfRow.getCell(1).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart4Dto part4 = new IndividualPlanPart4Dto();
                part4.setPage(sheetNum);
                part4.setPartPage(rowNum);
                for (int j = 1; j <= 4; j++) {
                    part4.setData(j, validator.validateCell(hssfRow, j));
                }
                result.add(part4);
            }
        }

        return result;
    }

    private void setPart4(HSSFSheet hssfSheet, List<IndividualPlanPart4Dto> listPart4, int sheetNum) {
        for (IndividualPlanPart4Dto part4Dto : listPart4) {
            if (part4Dto.getPage() == sheetNum) {
                for (int cellNum = 1; cellNum <= 4; cellNum++) {
                    setCell(part4Dto.getData(cellNum), hssfSheet, part4Dto.getPartPage(), cellNum);
                }
            }
        }
    }

    private List<IndividualPlanPart5Dto> getPart5(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart5Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART5.length; i++) {
            if (ApacheConstant.IND_PLAN_PART5[i][0] != sheetNum)
                continue;

            for (int rowNum = ApacheConstant.IND_PLAN_PART5[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART5[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(0) == null
                        || hssfRow.getCell(0).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart5Dto part5 = new IndividualPlanPart5Dto();
                part5.setPage(sheetNum);
                part5.setPartPage(rowNum);
                for (int j = 0; j <= 1; j++) {
                    part5.setData(j, validator.validateCell(hssfRow, j));
                }
                result.add(part5);
            }
        }

        return result;
    }

    private void setPart5(HSSFSheet hssfSheet, List<IndividualPlanPart5Dto> listPart5, int sheetNum) {
        for (IndividualPlanPart5Dto part5Dto : listPart5) {
            if (part5Dto.getPage() == sheetNum) {
                for (int cellNum = 0; cellNum <= 1; cellNum++) {
                    setCell(part5Dto.getData(cellNum), hssfSheet, part5Dto.getPartPage(), cellNum);
                }
            }
        }
    }

    private List<IndividualPlanPart6Dto> getPart6(HSSFSheet hssfSheet, int sheetNum) {
        List<IndividualPlanPart6Dto> result = new ArrayList<>();
        for (int i = 0; i < ApacheConstant.IND_PLAN_PART6.length; i++) {
            if (ApacheConstant.IND_PLAN_PART6[i][0] != sheetNum)
                continue;

            for (int rowNum = ApacheConstant.IND_PLAN_PART6[i][1];
                 rowNum <= ApacheConstant.IND_PLAN_PART6[i][2]; rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null || hssfRow.getCell(0) == null
                        || hssfRow.getCell(0).getCellType() == Cell.CELL_TYPE_BLANK) {
                    continue;
                }

                IndividualPlanPart6Dto part6 = new IndividualPlanPart6Dto();
                part6.setPage(sheetNum);
                part6.setPartPage(rowNum);
                for (int j = 0; j <= 1; j++) {
                    part6.setData(j, validator.validateCell(hssfRow, j));
                }
                result.add(part6);
            }
        }

        return result;
    }

    private void setPart6(HSSFSheet hssfSheet, List<IndividualPlanPart6Dto> listPart6, int sheetNum) {
        for (IndividualPlanPart6Dto part6Dto : listPart6) {
            if (part6Dto.getPage() == sheetNum) {
                for (int cellNum = 0; cellNum <= 1; cellNum++) {
                    setCell(part6Dto.getData(cellNum), hssfSheet, part6Dto.getPartPage(), cellNum);
                }
            }
        }
    }

    private void setCell(Object data, HSSFSheet hssfSheet, int rowNum, int cellNum) {
        if (hssfSheet.getRow(rowNum) == null){
            hssfSheet.createRow(rowNum);
        }

        if (hssfSheet.getRow(rowNum).getCell(cellNum) == null){
            hssfSheet.getRow(rowNum).createCell(cellNum);
        }

        if (data instanceof String) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((String) data);
        } else if (data instanceof Integer) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Integer) data);
        } else if (data instanceof Long) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Long) data);
        } else if (data instanceof Double) {
            hssfSheet.getRow(rowNum).getCell(cellNum).setCellValue((Double) data);
        }
    }
}
