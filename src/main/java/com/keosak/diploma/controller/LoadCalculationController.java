package com.keosak.diploma.controller;


import com.keosak.diploma.model.error.ResultDto;
import com.keosak.diploma.service.LoadCalculationService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//TODO
@Controller
@RequestMapping("/load")
public class LoadCalculationController {

    @Autowired
    LoadCalculationService service;

    @PostMapping("/file/add")
    public ResponseEntity<ResultDto> addFile(@RequestParam("file") MultipartFile file) {
        ResultDto resultDto = new ResultDto();
        try {
            service.addFile(file);
            resultDto.setAlertMsg("Файл було додано");
        } catch (Exception e){
            resultDto.setErrorMsg(e.getMessage());
        }

        return ResponseEntity.ok(resultDto);
    }

    @GetMapping("/file/get")
    public void getFile(HttpServletResponse response, Model model) throws IOException {
        try {
            response.addHeader("Content-Disposition", "attachment; filename=load.xls");
            IOUtils.copy(service.getFile(), response.getOutputStream());
            response.getOutputStream().flush();
        } catch (Exception e){
            response.reset();
            model.addAttribute("error", e.getMessage());
            response.sendRedirect("/load");
        }

    }

    @GetMapping
    public String page() {
        return "load";
    }



//    @PostMapping("/test/loadCalculation")
//    public ResponseEntity<ResponseDto<LoadCalculationHeadDto>> uploadPhoto(@RequestParam("file") MultipartFile file){
//        ResponseDto<LoadCalculationHeadDto> responseDto = new ResponseDto<>();
//        responseDto.setData(loadCalculationService.addFile(file));
//        return new ResponseEntity<>(responseDto, HttpStatus.OK);
//    }
//
//    @GetMapping("/test/load/findByIdUser")
//    public ResponseEntity<ResponseDto<LoadCalculationHeadDto>> findByIdUser(@RequestParam Long id){
//        ResponseDto<LoadCalculationHeadDto> responseDto = new ResponseDto<>();
//        responseDto.setData(loadCalculationService.findByIdUser(id));
//        return new ResponseEntity<>(responseDto, HttpStatus.OK);
//    }
}
