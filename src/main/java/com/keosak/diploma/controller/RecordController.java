package com.keosak.diploma.controller;

import com.keosak.diploma.model.error.ResultDto;
import com.keosak.diploma.service.RecordService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/record")
public class RecordController {

    @Autowired
    RecordService service;

    @GetMapping("/file/get")
    public ResponseEntity<ResultDto> getFile(HttpServletResponse response, @RequestParam String year) throws IOException {
        ResultDto resultDto = new ResultDto();
        try {
            response.addHeader("Content-Disposition", "attachment; filename=record.xls");
            IOUtils.copy(service.getFile(year), response.getOutputStream());
            response.getOutputStream().flush();
        } catch (Exception e){
            response.reset();
            response.sendRedirect("/record");
        }

        return ResponseEntity.ok(resultDto);
    }

    @GetMapping
    public String record() {
        return "record";
    }
}
