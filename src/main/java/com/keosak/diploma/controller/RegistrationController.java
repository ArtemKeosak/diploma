package com.keosak.diploma.controller;

import com.keosak.diploma.model.error.ErrorRegDto;
import com.keosak.diploma.entity.user.User;
import com.keosak.diploma.service.user.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService service;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("userForm") @Valid User userForm, BindingResult bindingResult, Model model) {

        List<ErrorRegDto> errors = service.registration(userForm);
        if (!errors.isEmpty()){
            for (ErrorRegDto error: errors){
                model.addAttribute(error.getNameField(), error.getNameError());
            }
            return "registration";
        }else if (bindingResult.hasErrors()) {
            return "registration";
        }

        return "redirect:/";
    }
}
