package com.keosak.diploma.controller;


import com.keosak.diploma.model.error.ResultDto;
import com.keosak.diploma.service.IndividualPlanService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//TODO
@Controller
@RequestMapping("/plan")
public class IndividualPlanController {

    @Autowired
    IndividualPlanService service;

    @PostMapping("/file/add")
    public ResponseEntity<ResultDto> addFile(@RequestParam("file") MultipartFile file) {
        ResultDto resultDto = new ResultDto();
        try {
            service.addFile(file);
            resultDto.setAlertMsg("Файл було додано");
        } catch (Exception e){
            resultDto.setErrorMsg(e.getMessage());
        }

        return ResponseEntity.ok(resultDto);
    }

    @GetMapping("/file/get")
    public void getFile(HttpServletResponse response) throws IOException {
        try {
            response.addHeader("Content-Disposition", "attachment; filename=plan.xls");
            IOUtils.copy(service.getFile(), response.getOutputStream());
            response.getOutputStream().flush();
        } catch (Exception e){
            response.reset();
            response.sendRedirect("/plan");
        }
    }

    @GetMapping
    public String record() {
        return "plan";
    }
}
