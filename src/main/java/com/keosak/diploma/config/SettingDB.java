package com.keosak.diploma.config;

import com.keosak.diploma.service.user.PositionService;
import com.keosak.diploma.service.user.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SettingDB implements InitializingBean {

    @Autowired
    PositionService position;
    @Autowired
    UserService service;

    @Override
    public void afterPropertiesSet() {
        service.setRole();
        position.setPosition();
        service.addSuperUser();
    }
}
