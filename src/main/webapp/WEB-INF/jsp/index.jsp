<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Головна</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="resources/css/index.css">
</head>
<body>
<div>
    <div class="menu-bar">
        <ul>
            <li><a href="/" class="disable">Головна</a></li>
        </ul>
        <ul>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/load">Навантаження</a></li>
                <li><a href="/plan">Індивідуальний план</a></li>
                <li><a href="/record">Звіт</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ADMIN')">
                <li><a href="/registration">Додати користувача</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/logout">Вийти</a></li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <li><a href="/login">Увійти</a></li>
            </sec:authorize>
        </ul>
    </div>

    <div class="hero">
        <div class="text">
            Слава Україні!
        </div>
        <img src="../../resources/img/ukr.png">
    </div>

</div>
</body>
</html>