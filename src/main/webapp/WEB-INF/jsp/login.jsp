<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Авторизація</title>
    <link href="resources/css/login.css" rel="stylesheet">
</head>

<body class="align">
<sec:authorize access="isAuthenticated()">
    <% response.sendRedirect("/"); %>
</sec:authorize>

<div class="login">

    <header class="login__header">
        <h2>
            <svg class="icon">
                <use xlink:href="#icon-lock"/>
            </svg>
            Sign In
        </h2>
    </header>

    <form action="/login" class="login__form" method="POST">

        <div>
            <label>Username</label>
            <input type="text" name="username" placeholder="username"
                   maxlength="20" required>
        </div>

        <div>
            <label>Password</label>
            <input type="password" name="password" placeholder="password"
                   maxlength="20" required>
        </div>

        <div>
            <input class="button" type="submit" value="Sign In">
        </div>

    </form>

</div>

<svg xmlns="http://www.w3.org/2000/svg" class="icons">
    <symbol id="icon-lock" viewBox="0 0 448 512">
        <path d="M400 224h-24v-72C376 68.2 307.8 0 224 0S72 68.2 72 152v72H48c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V272c0-26.5-21.5-48-48-48zm-104 0H152v-72c0-39.7 32.3-72 72-72s72 32.3 72 72v72z"/>
    </symbol>
</svg>

</body>
</html>
