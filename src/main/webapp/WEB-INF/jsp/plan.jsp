<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Індивідуальний план</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="resources/css/plan.css">
</head>
<body>
<div>
    <div class="menu-bar">
        <ul>
            <li><a href="/">Головна</a></li>
        </ul>
        <ul>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/load">Навантаження</a></li>
                <li><a href="/plan" class="disable">Індивідуальний план</a></li>
                <li><a href="/record">Звіт</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ADMIN')">
                <li><a href="/registration">Додати користувача</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/logout">Вийти</a></li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <li><a href="/login">Увійти</a></li>
            </sec:authorize>
        </ul>
    </div>

    <div class="hero">
        <div class="text">
            Індивідуальний план
        </div>
        <div class="btn">
            <form enctype="multipart/form-data" method="post" action="/plan/file/add"
                  id="form">
                <label for="fileUpload" class="button_record">Додати</label>
                <input type="file" name="file" accept=".xls"
                       id="fileUpload" style="display: none">
            </form>
            <a href="/plan/file/get">
                <button class="button_record" role="button">Завантажити</button>
            </a>
        </div>
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    document.getElementById("fileUpload").onchange = async function () {
        var data = new FormData()
        data.append('file', document.querySelector('input[type="file"]').files[0])

        let response = await fetch('/plan/file/add', {
            method: 'POST',
            body: data
        });
        let result = await response.json();

        if (result.errorMsg != null)
            swal({
                title: "Помилка",
                text: result.errorMsg,
                icon: 'error'
            });
        else if (result.alertMsg != null)
            swal({
                title: "Успіх",
                text: result.alertMsg,
                icon: 'success'
            });
    };
</script>
</body>
</html>