<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Реєстрація</title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="resources/css/registration.css">
</head>

<body>
<div>
    <div class="menu-bar">
        <ul>
            <li><a href="/">Головна</a></li>
        </ul>
        <ul>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/load">Навантаження</a></li>
                <li><a href="/plan">Індивідуальний план</a></li>
                <li><a href="/record">Звіт</a></li>
                <li><a href="/registration" class="disable">Додати користувача</a></li>
                <li><a href="/logout">Вийти</a></li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <li><a href="/login">Увійти</a></li>
            </sec:authorize>
        </ul>
    </div>
    <div class="hero">
        <form action="/registration" class="reg" method="POST" modelAttribute="userForm">
            <h3>Додати користувача</h3>
            <div class="d">
                <div>
                    ${errorUsername}
                </div>
                <div class="col">
                    <div class="field l">
                        <label>Username</label>
                        <input type="text" name="username" placeholder="Username"
                               minlength="5" maxlength="20" required>
                    </div>
                    <div class="field r">
                        <label>Посада</label>
                        <select name="idPosition">
                            <option value="1">Доцент</option>
                            <option value="2">Професор</option>
                            <option value="3">Асистент</option>
                            <option value="4">Ст. викладач</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d">
                <div>
                    ${errorPassword}
                    ${errorPasswordConfirm}
                </div>
                <div class="col">
                    <div class="field l">
                        <label>Password</label>
                        <input type="password" name="password" placeholder="Password"
                               minlength="5" maxlength="20" required>
                    </div>
                    <div class="field r">
                        <label>Confirm password</label>
                        <input type="password" name="passwordConfirm" placeholder="Confirm your password"
                               minlength="5" maxlength="20" required>
                    </div>
                </div>
            </div>
            <div class="d">
                <div>
                    ${errorFullName}
                </div>
                <div class="one">
                    <label>ПІБ</label>
                    <input type="text" name="fullName" placeholder="ПІБ"
                           minlength="2" maxlength="50" required>
                </div>
            </div>
            <div class="d">
                <div>
                    ${errorStartDate}
                    ${errorEndDate}
                </div>
                <div class="col">
                    <div class="field l">
                        <label>Дата початку контракту</label>
                        <input type="date" name="startContract" pattern="yyyy-MM-dd"
                               min="1980-01-01" max="2100-12-31" required>
                    </div>
                    <div class="field r">
                        <label>Дата кінця контракту</label>
                        <input type="date" name="endContract" pattern="yyyy-MM-dd"
                               min="1980-01-01" max="2100-12-31" required>
                    </div>
                </div>
            </div>
            <div class="d">
                <div>
                    ${errorShareRate}
                    ${errorMonthWorked}
                </div>
                <div class="col">
                    <div class="field l">
                        <label>Ставка</label>
                        <input type="number" name="shareRate" placeholder="Ставка"
                               min="0" max="5" step="0.1" required>
                    </div>
                    <div class="field r">
                        <label>Місяців працює</label>
                        <input type="number" name="monthsWorked" placeholder="Місяців працює"
                               min="0" max="480" required>
                    </div>
                </div>
            </div>
            <button type="submit" class="button">Додати користувача</button>
        </form>
    </div>
</div>
</body>
</html>