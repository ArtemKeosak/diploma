<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Звіт</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="resources/css/record.css">
</head>
<body>
<div>
    <div class="menu-bar">
        <ul>
            <li><a href="/">Головна</a></li>
        </ul>
        <ul>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/load">Навантаження</a></li>
                <li><a href="/plan">Індивідуальний план</a></li>
                <li><a href="/record" class="disable">Звіт</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ADMIN')">
                <li><a href="/registration">Додати користувача</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/logout">Вийти</a></li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <li><a href="/login">Увійти</a></li>
            </sec:authorize>
        </ul>
    </div>

    <div class="hero">
        <div class="text">
            Звіт
        </div>
        <div class="btn">
            <button class="button_record" role="button" onclick="select()">Завантажити</button>
        </div>
    </div>

</div>

<link href="https://cdn.jsdelivr.net/sweetalert2/4.1.5/sweetalert2.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/sweetalert2/4.1.5/sweetalert2.js"></script>
<script>
    async function select() {
        var list = {};
        list["2014/2015"] = "2014/2015";
        list["2015/2016"] = "2015/2016";
        list["2016/2017"] = "2016/2017";
        list["2017/2018"] = "2017/2018";
        list["2018/2019"] = "2018/2019";
        list["2019/2020"] = "2019/2020";
        list["2020/2021"] = "2020/2021";
        list["2021/2022"] = "2021/2022";
        list["2022/2023"] = "2022/2023";
        list["2023/2024"] = "2023/2024";
        list["2024/2025"] = "2024/2025";
        list["2025/2026"] = "2025/2026";
        list["2026/2027"] = "2026/2027";
        list["2027/2028"] = "2027/2028";
        list["2028/2029"] = "2028/2029";
        list["2029/2030"] = "2029/2030";

        swal({
            title: 'Виберіть рік для формування звіту',
            input: 'select',
            inputOptions: list,
            inputPlaceholder: 'Виберіть рік',
            showCancelButton: true,
            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value === "") {
                        reject('Потрібно вибрати рік');
                    } else {
                        resolve();
                    }
                });
            }
        }).then(function (result) {
            send(result)
        })
    }

    async function send(year) {
        window.open('record/file/get?year=' + year, '_blank')
    }
</script>
</body>
</html>